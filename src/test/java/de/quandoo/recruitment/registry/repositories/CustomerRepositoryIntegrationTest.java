package de.quandoo.recruitment.registry.repositories;

import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.Assert.assertEquals;


@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraTestConfig.class)
@EnableCassandraRepositories("de.quandoo.recruitment.registry.repository")
public class CustomerRepositoryIntegrationTest extends AbstractCuisineRepoTest {

    public static final String CUSTOMER_TABLE_NAME = "customer";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CassandraAdminOperations adminTemplate;

    @AfterClass
    public static void stopCassandraEmbedded() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

    @Before
    public void createTable() {
        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_TABLE_NAME), Customer.class, new HashMap<String, Object>());
    }

    @Test
    public void whenSavingCustomer_thenAvailableOnRetrieval() {

        final Customer customer = new Customer(UUID.randomUUID(), "Hans");
        customerRepository.saveCustomerIfNotExists(customer.getId(), customer.getName(), "");
        final Customer customers = customerRepository.findCustomerById(customer.getId());
        log.info(customerRepository.findCustomerById(customer.getId()).toString());
        assertEquals(customer.getId(), customers.getId());

    }


    @Test
    public void whenUpdatingCustomers_thenAvailableOnRetrieval() {

        final Customer customer = new Customer(UUID.randomUUID(), "John");
        customerRepository.save(customer);
        customer.setName("Maria");
        customerRepository.deleteCustomerById(customer.getId());
        customerRepository.save(customer);

        final Customer updatedCustomer = customerRepository.findCustomerById(customer.getId());
        assertEquals(customer.getName(), updatedCustomer.getName());

    }


    @Test(expected = NullPointerException.class)
    public void whenDeletingExistingCustomers_thenNotAvailableOnRetrieval() {

        final Customer customer = new Customer(UUID.randomUUID(), "Karl");
        customerRepository.save(customer);
        customerRepository.delete(customer);
        final Customer customerFromDb = customerRepository.findCustomerById(customer.getId());
        customerFromDb.getId();

    }

    @Test
    public void whenSavingCustomers_thenAllShouldAvailableOnRetrieval() {

        final Customer customerOne = new Customer(UUID.randomUUID(), "Alexander");
        final Customer customerTwo = new Customer(UUID.randomUUID(), " Judith");
        customerRepository.save(customerOne);
        customerRepository.save(customerTwo);
        assertEquals(customerRepository.count(), 2);

    }


    @After
    public void dropTable() {
        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_TABLE_NAME));
    }

}
