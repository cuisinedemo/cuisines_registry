package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.Customer_Cuisine;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CustomerCuisineRepository extends CassandraRepository<Customer_Cuisine, Long> {

    @Query("select *  from customer_cuisine where customer_id = ?0 ")
    Customer_Cuisine findCustomer_CuisineByCustomerId(UUID customerId);

    @Query("select *  from customer_cuisine where customer_id = ?0 and name= ?1")
    Customer_Cuisine findCustomer_CuisineByCustomerIdAndName(UUID customerId, String name);

    @Query("select cuisine_id from customer_cuisine where name = ?0 ")
    Iterable<UUID> findCustomer_CuisineByCuisineName(String name);

}
