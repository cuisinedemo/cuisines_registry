package de.quandoo.recruitment.registry.model;

import com.datastax.driver.core.DataType;
import lombok.AllArgsConstructor;
import lombok.ToString;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

/**
 * Table for querying the customer that ist registered to the
 * most number of cuisines.
 */
@AllArgsConstructor
@ToString
@Table("customer_cuisines_counter")
public class CustomerCuisinesCounter {

    @PrimaryKeyColumn(name = "customer_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED, ordering = Ordering.ASCENDING)
    @CassandraType(type = DataType.Name.UUID)
    private UUID customerId;

    @Column("number_of_cuisines_for_customer")
    @CassandraType(type = DataType.Name.COUNTER)
    private int numberOfCuisinesForCustomer = 0;

    public CustomerCuisinesCounter(UUID customerId) {
        this.customerId = customerId;

    }

    public UUID getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public int getNumberOfCuisinesForCustomer() {
        return this.numberOfCuisinesForCustomer;
    }

    public void setNumberOfCuisinesForCustomer(int numberOfCuisinesForCustomer) {
        this.numberOfCuisinesForCustomer = numberOfCuisinesForCustomer;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CustomerCuisinesCounter)) return false;
        final CustomerCuisinesCounter other = (CustomerCuisinesCounter) o;
        if (!other.canEqual(this)) return false;
        final Object this$customerId = this.getCustomerId();
        final Object other$customerId = other.getCustomerId();
        if (this$customerId == null ? other$customerId != null : !this$customerId.equals(other$customerId))
            return false;
        return this.getNumberOfCuisinesForCustomer() == other.getNumberOfCuisinesForCustomer();
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $customerId = this.getCustomerId();
        result = result * PRIME + ($customerId == null ? 43 : $customerId.hashCode());
        result = result * PRIME + this.getNumberOfCuisinesForCustomer();
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof CustomerCuisinesCounter;
    }
}
