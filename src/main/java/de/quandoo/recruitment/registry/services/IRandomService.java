package de.quandoo.recruitment.registry.services;


import de.quandoo.recruitment.registry.model.dto.CuisineDTO;
import de.quandoo.recruitment.registry.model.dto.CustomerDTO;

import java.util.UUID;

/**
 * see implementation
 */
public interface IRandomService {

    double getRandomPrice();

    CustomerDTO getRandomCustomer();

    CuisineDTO getRandomCusisine();

    UUID getRandomId();

}
