package de.quandoo.recruitment.registry.web.controllers;

import de.quandoo.recruitment.registry.api.CuisinesRegistryExtraOps;
import de.quandoo.recruitment.registry.api.PaginationOperations;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.dto.KafkaCuisineCustomersDTO;
import de.quandoo.recruitment.registry.web.exceptions.CuisineNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
public class CusinesRegistryRestController {


    @Autowired
    CuisinesRegistryExtraOps cuisineRegistry;

    @Autowired
    private PaginationOperations paginationOperations;

    @GetMapping(value = "/popular/get", params = {"page", "size"}, produces = "application/json")
    public Slice<Cuisine> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {

        Slice<Cuisine> resultPage = paginationOperations.findPaginated(page, size);
        if (page > resultPage.getNumber()) {
            throw new CuisineNotFoundException();
        }
        return resultPage;
    }

    /**
     * publish the registration of  a customer to a cuisine to Kafka. This topic
     * will be used by SPARK to count the popular cuisines.  This is an alternative
     * to the cassandra counter and rethinkdb
     *
     * @param cuisineId
     * @param customerId
     * @return
     */
    @GetMapping(value = "/register/{customerId}/{cuisineId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> test(@PathVariable String cuisineId, @PathVariable String customerId) {
        log.info("Registering customer{} to cuisine {}  ", customerId, cuisineId);
        KafkaCuisineCustomersDTO temp = new KafkaCuisineCustomersDTO();
        temp.setCuisineId(cuisineId);
        temp.setCustomerId(customerId);
        temp.setRegisteredToCuisineOnDate(new Date());
        cuisineRegistry.publishToKafka(temp);
        return new ResponseEntity<>(temp.toString(), HttpStatus.OK);
    }

    @GetMapping(value = "/popular/{howMany}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Cuisine>> getTop(@PathVariable int howMany) {
        List<Cuisine> topCuisines = cuisineRegistry.topCuisines(howMany);
        if (topCuisines == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (topCuisines.isEmpty()) {
            return new ResponseEntity<>(topCuisines, HttpStatus.NOT_FOUND);
        } else return new ResponseEntity<>(topCuisines, HttpStatus.OK);
    }

}