package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.services.IRandomService;
import de.quandoo.recruitment.registry.services.RandomServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.test.context.ActiveProfiles;

/**
 * Configuration class for the embedded cassandra server. The
 * embedded server is used only for the tests. For the web application
 * you should call "docker-compose up" to start cassandra before running
 * ./gradlew bootRun
 */
@Slf4j
@Configuration
@ActiveProfiles("test")
@PropertySource("classpath:cassandra-test.properties")
public class CassandraTestConfig extends AbstractCassandraConfiguration {

    @Value("${cassandra.test.hosts}")
    String cassandraHosts;

    @Value("${cassandra.test.port}")
    String cassandraPort;

    @Value("${cassandra.test.keyspace}")
    String cassandraKeySpace;

    @Override
    public String getKeyspaceName() {
        return cassandraKeySpace;
    }

    @Bean
    public CassandraClusterFactoryBean cluster() {
        try {
            EmbeddedCassandraServerHelper.startEmbeddedCassandra(EmbeddedCassandraServerHelper.DEFAULT_CASSANDRA_YML_FILE, 1000000L);
            EmbeddedCassandraServerHelper.getCluster().getConfiguration().getSocketOptions().setReadTimeoutMillis(1000000);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("Can't start Embedded Cassandra", e);
        }
        return super.cluster();
    }

    @Bean(name = "randomService")
    public IRandomService randomService() {
        return new RandomServiceImpl();
    }


    @Override
    protected int getPort() {
        return Integer.parseInt(cassandraPort);
    }

    @Override
    public String[] getEntityBasePackages() {
        return new String[]{"de.quandoo.recruitment.registry.model"};
    }

    @Override
    public SchemaAction getSchemaAction() {
        return SchemaAction.CREATE_IF_NOT_EXISTS;
    }


}
