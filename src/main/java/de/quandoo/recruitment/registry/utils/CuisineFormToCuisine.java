package de.quandoo.recruitment.registry.utils;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.web.CuisineForm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * util class for the controller
 */
@Component
public class CuisineFormToCuisine implements Converter<CuisineForm, Cuisine> {

    @Override
    public Cuisine convert(CuisineForm cuisineForm) {
        Cuisine cuisine = new Cuisine();
        if (cuisineForm.getCuisineId() != null && !StringUtils.isEmpty(cuisineForm.getCuisineId())) {
            cuisine.setCuisineId(cuisineForm.getCuisineId());
        }
        if (cuisineForm.getName() != null && !StringUtils.isEmpty(cuisineForm.getName())) {
            cuisine.setName(cuisineForm.getName());
        }
        return cuisine;
    }
}
