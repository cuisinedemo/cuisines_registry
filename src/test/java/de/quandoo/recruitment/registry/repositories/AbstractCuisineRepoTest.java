package de.quandoo.recruitment.registry.repositories;


import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.services.IRandomService;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.spring.EmbeddedCassandra;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.util.Properties;

/**
 * Base class for all tests.  Takes care of DB initialization
 */
@Slf4j
@Profile("test")
@EmbeddedCassandra
@TestPropertySource(locations = "classpath:cassandra-test.properties")
@ComponentScan(basePackages = {"de.quandoo.recruitment.registry", "de.quandoo.recruitment.registry.repository"})
@Import(CassandraTestConfig.class)
public abstract class AbstractCuisineRepoTest {

    static Session session = null;

    @Value("${cassandra.hosts}")
    String cassandraHosts;

    @Value("${cassandra.port}")
    String cassandraPort;

    @Autowired
    IRandomService randomService;

    @Value("${cassandra.keyspace}")
    String cassandraKeySpace;

    /**
     * Initializes cassandra using the test properties
     */
    @BeforeClass
    public static void initCassandra() {
        try {
            Properties cassandraTestProperties = new Properties();
            cassandraTestProperties.load(AbstractCuisineRepoTest.class.getClassLoader().getResourceAsStream("cassandra-test.properties"));

            final String cassandraKeyspace = cassandraTestProperties.getProperty("cassandra.test.keyspace");

            EmbeddedCassandraServerHelper.startEmbeddedCassandra(20000);

            log.info("Connect to embedded db");
            Cluster cluster = Cluster.builder().addContactPoints(cassandraTestProperties.getProperty("cassandra.test.hosts")).withPort(Integer.parseInt(cassandraTestProperties.getProperty("cassandra.test.port"))).build();
            session = cluster.connect();

            session.execute("create keyspace if not exists " + cassandraKeyspace + " WITH replication = {'class':'SimpleStrategy', 'replication_factor':1};");
            session.execute("use " + cassandraKeyspace + ";");
            log.info("Initialized keyspace");

        } catch (TTransportException e) {
            log.error(e.toString());
        } catch (IOException e) {
            log.error(e.toString());
        }
    }

    /**
     * wait for the embedded cassandra to start ...
     */
    @Before
    public void initTest() {
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error(e.toString());
        }
    }


}