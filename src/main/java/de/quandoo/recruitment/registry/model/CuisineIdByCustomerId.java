package de.quandoo.recruitment.registry.model;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

/**
 * Table for querying customers id by cuisine ids
 */
@Table
public class CuisineIdByCustomerId {
    @PrimaryKeyColumn(name = "customer_id", ordinal = 0, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private UUID customerId;

    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private String cuisineId;

}
