package de.quandoo.recruitment.registry.utils;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.web.CuisineForm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * util class for the controller
 */
@Component
public class CuisineToCuisineForm implements Converter<Cuisine, CuisineForm> {
    @Override
    public CuisineForm convert(Cuisine cuisine) {
        CuisineForm cuisineForm = new CuisineForm();
        cuisineForm.setCuisineId(cuisine.getCuisineId());
        cuisineForm.setName(cuisine.getName());
        return cuisineForm;
    }
}
