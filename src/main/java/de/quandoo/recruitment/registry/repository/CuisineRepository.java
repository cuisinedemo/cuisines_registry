package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.Cuisine;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CuisineRepository extends CassandraRepository<Cuisine, Long> {

    @Query("select * from cuisine where cuisine_id = ?0 ")
    Cuisine findCuisineByCuisineID(UUID cuisine_id);

    @Query("select * from cuisine where cuisine_id = ?0 and name= ?1")
    Cuisine findCuisineByCuisineIdAndName(UUID cuisine_id, String name);

    @Query("select * from cuisine where name = ?0 ")
    Iterable<Cuisine> findCuisineByName(String name);

    @Query("select * from cuisine where tags CONTAINS = ?0 ")
    Iterable<Cuisine> findCuisineByTag(String foodType);

    @Query("INSERT INTO cuisine ( cuisine_id, name ) " + "  values ( ?0 ,  ?1 ) IF NOT EXISTS")
    Cuisine saveCuisineIfNotExists(UUID cuisineId, String name);

    @Query("delete   from cuisine where cuisine_id = ?0 ")
    void deleteCuisineByCuisineId(UUID cuisineId);
}
