package de.quandoo.recruitment.registry.utils;

import de.quandoo.recruitment.registry.config.CassandraConfig;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.dto.CuisineDTO;
import de.quandoo.recruitment.registry.model.dto.CustomerDTO;
import de.quandoo.recruitment.registry.services.IRandomService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Utility class for creating random cuisine/customer objects.
 */
@Component
public class CuisinesCreator {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CassandraConfig.class);

    @Autowired
    IRandomService randomService;

    /**
     * utility method for creating random cuisine objects for test purposes
     *
     * @param numberOfCuisines
     * @return
     */
    public Set<Cuisine> createRandomCuisines(int numberOfCuisines) {
        Set<Cuisine> allCuisinesForThisCustomer = new HashSet<>();
        for (int i = 0; i < numberOfCuisines; i++) {
            final Cuisine temp = new Cuisine();
            CuisineDTO cuisineDTO = randomService.getRandomCusisine();
            temp.setDishPrices(cuisineDTO.getDishPrices());
            temp.setDishTypes(cuisineDTO.getDishTypes());
            temp.setFoodTypes(cuisineDTO.getFoodTypes());
            temp.setName(cuisineDTO.getName());
            temp.setCuisineId(cuisineDTO.getCuisineId());
            log.debug("created {}", temp.toString());
            allCuisinesForThisCustomer.add(temp);
        }
        return allCuisinesForThisCustomer;
    }

    /**
     * utility method for creating random customers objects for test purposes
     *
     * @param numberOfCustomers how many customers should be created
     * @return a set of fake customers
     */
    public Set<Customer> createRandomCustomers(int numberOfCustomers) {
        Set<Customer> fakeCustomers = new HashSet<>();
        for (int i = 0; i < numberOfCustomers; i++) {
            final Customer temp = new Customer();
            CustomerDTO customerDTO = randomService.getRandomCustomer();
            temp.setName(customerDTO.getName());
            temp.setId(customerDTO.getId());
            temp.setEmail(customerDTO.getEmail());
            log.debug("created {}", temp.toString());
            fakeCustomers.add(temp);
        }
        return fakeCustomers;
    }


}
