package de.quandoo.recruitment.registry.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;


/**
 * Config class for the rethinkdb
 */
@ActiveProfiles("dev")
@Configuration
@PropertySource("classpath:rethinkdb.properties")
public class RethinkDbConfig {

    @Value("${rethinkdb.host}")
    String rethinkDbHost;

    @Value("${rethinkdb.port}")
    String rethinkDbPort;

    @Value("${rethinkdb.database}")
    String rethinkDbDatabase;


    @Autowired
    private Environment env;

    @Bean
    public RethinkDBConnectionFactory connectionFactory() {
        return new RethinkDBConnectionFactory(rethinkDbHost);
    }

    @Bean
    DbInitializer dbInitializer() {
        return new DbInitializer();
    }
}