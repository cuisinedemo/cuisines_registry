package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.CuisineCustomerByRegDate;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuisineCustomerByRegDateRepository extends CassandraRepository<CuisineCustomerByRegDate, Long> {

}
