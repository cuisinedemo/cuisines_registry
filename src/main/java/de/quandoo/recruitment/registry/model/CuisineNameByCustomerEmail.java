package de.quandoo.recruitment.registry.model;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

/**
 * Table for querying cuisines by customer email
 */
@Table
public class CuisineNameByCustomerEmail {
    @PrimaryKeyColumn(name = "customer_email", ordinal = 0, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private String customerEmail;

    @PrimaryKeyColumn(name = "cuisine_name", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private String cuisineName;

}
