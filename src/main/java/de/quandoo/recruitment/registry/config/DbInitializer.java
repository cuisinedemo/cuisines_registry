package de.quandoo.recruitment.registry.config;


import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/**
 * initialing stuff, checks if the rethinkdb table exists on booting,
 * if not create a new table.
 */
@PropertySource({"classpath:rethinkdb.properties"})
public class DbInitializer implements InitializingBean {

    private static final RethinkDB r = RethinkDB.r;
    @Value("${rethinkdb.host}")
    String rethinkDbHost;
    @Value("${rethinkdb.port}")
    String rethinkDbPort;
    @Value("${rethinkdb.database}")
    String rethinkDbDatabase;
    @Value("${rethinkdb.recreatedb}")
    String rethinkDbRecreateDb;
    @Value("${rethinkdb.table.name}")
    String tableName;
    @Autowired
    private RethinkDBConnectionFactory connectionFactory;

    @Override
    public void afterPropertiesSet() {
        createDb();
    }

    private void createDb() {
        Connection connection = connectionFactory.createConnection();
        List<String> dbList = r.dbList().run(connection);
        if (dbList.contains(rethinkDbDatabase)) {
            r.dbDrop(rethinkDbDatabase).run(connection);
        }

        r.dbCreate(rethinkDbDatabase).run(connection);

        List<String> tables = r.db(rethinkDbDatabase).tableList().run(connection);
        if (!tables.contains(tableName)) {
            r.db(rethinkDbDatabase).tableCreate(tableName).run(connection);
            r.db(rethinkDbDatabase).table(tableName).indexCreate("count").run(connection);
        }
    }
}