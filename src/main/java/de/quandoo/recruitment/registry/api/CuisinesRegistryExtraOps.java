package de.quandoo.recruitment.registry.api;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.dto.KafkaCuisineCustomersDTO;

import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * extended interface of the original quandoo interface:  that one should
 * not be changed.
 */
public interface CuisinesRegistryExtraOps extends CuisinesRegistry {

    Set<Cuisine> createRandomCuisinesWithRandomCustomers(int howManyCuisines, int maxCustomersPerCuisine);

    List<Cuisine> listAllCuisines();

    Cuisine findCuisineById(UUID cuisineId);

    Cuisine save(Cuisine cuisine);

    Long getNumberOfCustomersForThisCuisine(UUID cuisineId);

    void deleteAllCuisines();

    void deleteAllData();

    void deleteCuisineById(UUID cuisineId);

    String publishToKafka(KafkaCuisineCustomersDTO kafkaCuisineCustomersDTO);


}
