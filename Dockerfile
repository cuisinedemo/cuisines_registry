# docker build --no-cache ./ -t cuisine-registry
FROM openjdk:8
LABEL maintainer="brandao.vidal@gmail.com"
VOLUME /tmp

# let port be reachable outside the container
EXPOSE 10434
# The application's jar file
ARG JAR_FILE=build/libs/cuisines-registry-1.0.0-SNAPSHOT.jar
# Add the application's jar to the container
ADD ${JAR_FILE} cuisines-registry.jar
# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/cuisine-registry.jar"]
