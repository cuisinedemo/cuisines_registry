
 ( a running web interface from this tool can be seen at http://brandao-vidal.info:10434 )



                                 Cuisine Registry Demo 


Focus of the implementation was the Backend functionality. Nevertheless a quick-and-dirty front end was set in order to see some results without running tests all the time. 
All but one table are in a Cassandra Db. An extra table for the top cuisines is in the RethinkDb ( Cassandra cannot sort tables by the object COUNTER).
 
    To start the tool 
-	Start Cassandra ,Kafka and Rethinkdb  from the code base dir with “ docker-compose up”
-	Wait for the DBs to initialize.
-	To start the integration tests: call ./gradlew clean test 
-	To start the web interface:  call ./gradlew bootRun


There are about 21 integration tests to go through. Some repositories are not tested, as to write a test for all repos would be too much overhead – the table Cuisine itself is split in several tables , as in Cassandra we need one table per query. The test class “QuandooChallengeTest.java” contains the recruitment tasks tests. 



              Tasks given by the recruitment team: 

**1)	Customer registers to several Cuisines:**
-	Go to http://localhost:10434/createCuisines  
-	Create some random cuisines with customers registered to it. For each Cuisine
created a random nr. of Customers will register themselves to it.
-	The results can be seen at http://localhost:10434/list.  If you click at the link “view”
on any cuisine object you can see how many customers were registered to it.
 
 
Although the backend is made for  supporting lots of data, be aware that the front end 
Was not tested and has on pagination functionality – the input on  the fields are limited to 50.

The Tests ( java classes ) do not have this limitation, feel free to play around.  The DB holding the ids of cuisines to be sorted ( top cuisines) can process million calls per second ( in the right cluster configuration ) 
. You can administer the Rethinkdb‚ at  http://localhost:9499/   

The tests run with an embedded cassandra instance and the data is lost after the tests.



**2)	Get top “n” cuisines ( most popular by number of customers)**
-	Go to http://localhost:10434/createCuisines 
-	Create some random cuisines with customers registered to it
-	Get the sorted results ( by popularity) from the Rest interface at http://localhost:10434/popular/<how-many>



Each Time a customer register to a cuisine  a message is published to a Kafka topic, containing the customer id , cuisine id and the registration date.   

        ALTERNATIVE TO Rethinkdb and Cassandra COUNTER 
A SPARK job connects to the Kafka stream in a pre-defined interval , summing all counts for a certain cuisine and sorting in descending order.   
This tool is implemented in anotther repo ( cannot start on the same context as the
 cuisine registry app,  git clone the project "topcuisines"  and start the main class  - there is only one class in that repo  ), Once started this tool fetches  cuisine-customers objects from Kafka, aggregating them by cuisiine Key.  As result a sorted list of most popular cuisines is printed to the console.  This is just a proof-of-concept, Note that Kafka has to be runnning for the spark job to process the topic.  If after a while there is no messages from the cuisine registry took, spark starves and and the tool exits.    The list of poular cuisines should be saved to cassandra, I did not have the time for that. .


                  Git repo for the Spark Service Tool:  
 https://gitlab.com/cuisinedemo/topcuisines
 

                  REST INTERFACE  for publishing to Kafka
		 http://localhost:10434/register/{customerId}/{cuisineId}
This is just for quick testing the registering to a cuisine by a customer using the REST interface.    You can also use the Batch insert in the main web site to create random cuisines with customers registerd to it ( limit to 50  cuisines with max. 50 customers each, each time you create them , as I did not implement pagination for this ...)
 
 
                                        ISSUES
The message “java.io.IOException: Connection reset by peer”  when running tests is normal.  It is due to the fact that the tests close the connection to Cassandra when finshed, but it does not close properly – that is because Cassandra was not supposed to word like this, in embedded mode.  It throws when all tests are already finished.

**LOMBOK:**  as different IDEs can have issues with Lombok +gradle , I delomboked most classes
