package de.quandoo.recruitment.registry;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Slf4j
@SpringBootApplication
@EnableCassandraRepositories
public class CuisineRegistryApplication {


    public static void main(String[] args) {
        SpringApplication.run(CuisineRegistryApplication.class, args);
    }

}

