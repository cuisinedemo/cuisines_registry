package de.quandoo.recruitment.registry.repositories;

import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.dto.CuisineDTO;
import de.quandoo.recruitment.registry.repository.CuisineRepository;
import de.quandoo.recruitment.registry.services.IRandomService;
import org.apache.cassandra.exceptions.ConfigurationException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Cuisine Repo test.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraTestConfig.class)
@EnableCassandraRepositories("de.quandoo.recruitment.registry.repository")
public class CuisineRepositoryIntegrationTest extends AbstractCuisineRepoTest {

    public static final String DATA_TABLE_NAME = "cuisine";

    @Autowired
    IRandomService randomService;

    @Autowired
    private CuisineRepository cuisineRepository;

    @Autowired
    private CassandraAdminOperations adminTemplate;

    @AfterClass
    public static void stopCassandraEmbedded() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

    @Before
    public void createTable() throws ConfigurationException {
        adminTemplate.createTable(true, CqlIdentifier.of(DATA_TABLE_NAME), Cuisine.class, new HashMap<String, Object>());
    }

    @Test
    public void whenSavingCuisine_thenAvailableOnRetrieval() {

        final Cuisine cuisine = new Cuisine(UUID.randomUUID(), "Italian");
        CuisineDTO cuisineDTO = randomService.getRandomCusisine();
        cuisine.setDishPrices(cuisineDTO.getDishPrices());
        cuisineRepository.save(cuisine);
        final Cuisine cuisineFromDb = cuisineRepository.findCuisineByCuisineID(cuisine.getCuisineId());
        assertEquals(cuisine.getCuisineId(), cuisineFromDb.getCuisineId());

    }

    @Test
    public void whenUpdatingCuisines_thenAvailableOnRetrieval() {

        final Cuisine cuisine = new Cuisine(UUID.randomUUID(), "French");
        Cuisine fromDB = cuisineRepository.save(cuisine);
        fromDB.setName("Indonesian");

        cuisineRepository.delete(cuisine);
        final Cuisine updatedCuisine = cuisineRepository.findCuisineByCuisineID(cuisine.getCuisineId());
        assertEquals(cuisine.getCuisineId(), updatedCuisine.getCuisineId());

    }

    @Test(expected = NullPointerException.class)
    public void whenDeletingExistingCuisines_thenNotAvailableOnRetrieval() {

        final Cuisine cuisine = new Cuisine(UUID.randomUUID(), "Thai");
        cuisineRepository.save(cuisine);
        cuisineRepository.delete(cuisine);
        final Cuisine cuisineFromDb = cuisineRepository.findCuisineByCuisineIdAndName(cuisine.getCuisineId(), cuisine.getName());
        cuisineFromDb.getCuisineId();

    }


    @Test
    public void whenSavingCuisines_thenAllShouldAvailableOnRetrieval() {

        final Cuisine customerOne = new Cuisine(UUID.randomUUID(), "Mexican");
        final Cuisine customerTwo = new Cuisine(UUID.randomUUID(), " Lebanese");
        cuisineRepository.save(customerOne);
        cuisineRepository.save(customerTwo);
        cuisineRepository.count();
        assertEquals(cuisineRepository.count(), 2);

    }

    @After
    public void dropTable() {
        adminTemplate.dropTable(CqlIdentifier.of(DATA_TABLE_NAME));
    }

}
