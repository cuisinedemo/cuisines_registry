package de.quandoo.recruitment.registry.model.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;

/**
 * Base class for DTOs. Overrides toString method to print json format
 */
@EqualsAndHashCode
public class BaseEntity {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(BaseEntity.class);

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            jsonString = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            log.error(e.toString());
        }
        return jsonString;
    }

}
