package de.quandoo.recruitment.registry.config;

import com.datastax.driver.core.Session;
import de.quandoo.recruitment.registry.services.IRandomService;
import de.quandoo.recruitment.registry.services.RandomServiceImpl;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraEntityClassScanner;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.CassandraAdminTemplate;
import org.springframework.data.cassandra.core.convert.CassandraConverter;
import org.springframework.data.cassandra.core.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.SimpleUserTypeResolver;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.convert.CustomConversions;
import org.springframework.test.context.ActiveProfiles;

/**
 * Configuration class for the cassandra cluster.
 */
@ActiveProfiles("dev")
@Configuration
@PropertySource("classpath:cassandra.properties")
@EnableCassandraRepositories(basePackages = "de.quandoo.recruitment.registry.repository")
public class CassandraConfig extends AbstractCassandraConfiguration {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CassandraConfig.class);

    @Value("${cassandra.hosts}")
    String cassandraHosts;

    @Value("${cassandra.port}")
    String cassandraPort;

    @Value("${cassandra.keyspace}")
    String cassandraKeySpace;


    @Override
    public String getKeyspaceName() {
        return cassandraKeySpace;
    }


    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
        cluster.setContactPoints(cassandraHosts);
        cluster.setPort(Integer.parseInt(cassandraPort));
        return cluster;
    }

    @Bean(name = "randomService")
    public IRandomService randomService() {
        return new RandomServiceImpl();
    }

    @Override
    protected int getPort() {
        return Integer.parseInt(cassandraPort);
    }

    @Override
    public String[] getEntityBasePackages() {
        return new String[]{"de.quandoo.recruitment.registry.model", "de.quandoo.recruitment.registry.repository"};
    }

    @Override
    public SchemaAction getSchemaAction() {
        return SchemaAction.CREATE_IF_NOT_EXISTS;
    }

    @Bean
    public CassandraMappingContext cassandraMapping() {
        BasicCassandraMappingContext mappingContext = new BasicCassandraMappingContext();
        try {
            mappingContext.setInitialEntitySet(CassandraEntityClassScanner.scan(getEntityBasePackages()));
        } catch (ClassNotFoundException e) {
            log.error(e.toString());
        }
        CustomConversions customConversions = customConversions();
        mappingContext.setCustomConversions(customConversions);
        mappingContext.setSimpleTypeHolder(customConversions.getSimpleTypeHolder());
        mappingContext.setUserTypeResolver(new SimpleUserTypeResolver(cluster().getObject(), getKeyspaceName()));
        return mappingContext;
    }

    @Bean
    @Override
    public CassandraConverter cassandraConverter() {
        MappingCassandraConverter mappingCassandraConverter = new MappingCassandraConverter(cassandraMapping());
        mappingCassandraConverter.setCustomConversions(customConversions());
        return mappingCassandraConverter;
    }

    /**
     * initialize the db here, create the keyspace if not existent
     *
     * @return
     */
    @Bean
    @Override
    public CassandraSessionFactoryBean session() {

        CassandraSessionFactoryBean session = new CassandraSessionFactoryBean();
        session.setCluster(cluster().getObject());
        session.setConverter(cassandraConverter());
        session.setKeyspaceName(getKeyspaceName());
        session.setSchemaAction(getSchemaAction());
        session.setStartupScripts(getStartupScripts());
        session.setShutdownScripts(getShutdownScripts());
        log.info("Initializing keyspace");
        Session session1 = cluster().getObject().connect();
        session1.execute("create keyspace if not exists \"" + cassandraKeySpace + "\" WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 3};");
        session1.execute("use " + cassandraKeySpace + ";");


        return session;
    }

    @Bean
    @Override
    public CassandraAdminOperations cassandraTemplate() {
        return new CassandraAdminTemplate(session().getObject(), cassandraConverter());
    }


}