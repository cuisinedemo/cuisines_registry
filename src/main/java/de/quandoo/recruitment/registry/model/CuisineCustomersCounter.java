package de.quandoo.recruitment.registry.model;

import com.datastax.driver.core.DataType;
import lombok.AllArgsConstructor;
import lombok.ToString;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

/**
 * Table for keeping a distributed count variable ( type COUNTER )
 * for the number of registered customers for a cuisine
 */
@AllArgsConstructor
@ToString
@Table("cuisine_customers_counter")
public class CuisineCustomersCounter {

    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED, ordering = Ordering.ASCENDING)
    @CassandraType(type = DataType.Name.UUID)
    private UUID cuisineId;

    @Column("number_of_registered_customers")
    @CassandraType(type = DataType.Name.COUNTER)
    private int numberOrRegisteredCustomers = 0;


    public UUID getCuisineId() {
        return this.cuisineId;
    }

    public void setCuisineId(UUID cuisineId) {
        this.cuisineId = cuisineId;
    }

    public int getNumberOrRegisteredCustomers() {
        return this.numberOrRegisteredCustomers;
    }

    public void setNumberOrRegisteredCustomers(int numberOrRegisteredCustomers) {
        this.numberOrRegisteredCustomers = numberOrRegisteredCustomers;
    }


    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CuisineCustomersCounter)) return false;
        final CuisineCustomersCounter other = (CuisineCustomersCounter) o;
        if (!other.canEqual(this)) return false;
        final Object this$cuisineId = this.getCuisineId();
        final Object other$cuisineId = other.getCuisineId();
        if (this$cuisineId == null ? other$cuisineId != null : !this$cuisineId.equals(other$cuisineId)) return false;
        return this.getNumberOrRegisteredCustomers() == other.getNumberOrRegisteredCustomers();
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $cuisineId = this.getCuisineId();
        result = result * PRIME + ($cuisineId == null ? 43 : $cuisineId.hashCode());
        result = result * PRIME + this.getNumberOrRegisteredCustomers();
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof CuisineCustomersCounter;
    }
}
