package de.quandoo.recruitment.registry.model;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static java.util.stream.Collectors.toSet;

/**
 * Table that maps cuisine to customers.It contains all customers(ids) a
 * that are registered to a cuisine.
 */
//@Builder
@Table
public class Cuisine_Customer {

    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private UUID cuisineId;

    @PrimaryKeyColumn(name = "name", ordinal = 1, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private String name;


    @Column
    private Set<UUID> customers = new HashSet<>();

    public Cuisine_Customer(final Cuisine cuisine, final Customer customer) {
        this.cuisineId = cuisine.getCuisineId();
        this.customers.add(customer.getId());
    }

    public Cuisine_Customer(final Cuisine cuisine, final Set<Customer> customers) {
        this.cuisineId = cuisine.getCuisineId();
        this.name = cuisine.getName();
        this.customers.addAll(customers.stream().map(customer -> customer.getId()).collect(toSet()));
    }

    public UUID getCuisineId() {
        return this.cuisineId;
    }

    public void setCuisineId(UUID cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UUID> getCustomers() {
        return this.customers;
    }

    public void setCustomers(Set<UUID> customers) {
        this.customers = customers;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Cuisine_Customer)) return false;
        final Cuisine_Customer other = (Cuisine_Customer) o;
        if (!other.canEqual(this)) return false;
        final Object this$cuisineId = this.getCuisineId();
        final Object other$cuisineId = other.getCuisineId();
        if (this$cuisineId == null ? other$cuisineId != null : !this$cuisineId.equals(other$cuisineId)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$customers = this.getCustomers();
        final Object other$customers = other.getCustomers();
        return this$customers == null ? other$customers == null : this$customers.equals(other$customers);
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $cuisineId = this.getCuisineId();
        result = result * PRIME + ($cuisineId == null ? 43 : $cuisineId.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $customers = this.getCustomers();
        result = result * PRIME + ($customers == null ? 43 : $customers.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Cuisine_Customer;
    }

    public String toString() {
        return "Cuisine_Customer(cuisineId=" + this.getCuisineId() + ", name=" + this.getName() + ", customers=" + this.getCustomers() + ")";
    }
}
