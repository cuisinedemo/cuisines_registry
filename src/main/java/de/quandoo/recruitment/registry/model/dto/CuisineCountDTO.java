package de.quandoo.recruitment.registry.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Utility class for rest operations
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CuisineCountDTO extends BaseEntity {

    Date countedOnDate;
    private String id;
    private String cuisine_id;
    private Long count;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCuisine_id() {
        return this.cuisine_id;
    }

    public void setCuisine_id(String cuisine_id) {
        this.cuisine_id = cuisine_id;
    }

    public Long getCount() {
        return this.count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Date getCountedOnDate() {
        return countedOnDate;
    }

    public void setCountedOnDate(Date countedOnDate) {
        this.countedOnDate = countedOnDate;
    }


}
