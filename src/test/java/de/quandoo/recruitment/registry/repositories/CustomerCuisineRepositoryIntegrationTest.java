package de.quandoo.recruitment.registry.repositories;

import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineCustomersCounter;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.CustomerCuisinesCounter;
import de.quandoo.recruitment.registry.model.Customer_Cuisine;
import de.quandoo.recruitment.registry.repository.CuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerCuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerCuisinesCounterRepository;
import de.quandoo.recruitment.registry.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;


/**
 * CustomerCuisine Table maps customers to cuisines.
 */
@Slf4j
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraTestConfig.class)
@EnableCassandraRepositories("de.quandoo.recruitment.registry.repository")
public class CustomerCuisineRepositoryIntegrationTest extends AbstractCuisineRepoTest {


    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String CUISINE_TABLE_NAME = "cuisine";
    public static final String CUSTOMER_CUISINE_TABLE_NAME = "customer_cuisine";
    public static final String CUISINE_CUSTOMERS_COUNTER_TABLE_NAME = "cuisine_customers_counter";
    public static final String CUSTOMER_CUISINES_COUNTER_TABLE_NAME = "customer_cuisines_counter";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CuisineRepository cuisineRepository;

    @Autowired
    private CustomerCuisineRepository customerCuisineRepository;

    @Autowired
    private CustomerCuisinesCounterRepository customerCuisinesCounterRepository;

    @Autowired
    private CassandraAdminOperations adminTemplate;


    @AfterClass
    public static void stopCassandraEmbedded() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

    @Before
    public void createTable() {

        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_TABLE_NAME), Customer.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_TABLE_NAME), Cuisine.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_CUISINE_TABLE_NAME), Customer_Cuisine.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_CUSTOMERS_COUNTER_TABLE_NAME), CuisineCustomersCounter.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_CUISINES_COUNTER_TABLE_NAME), CustomerCuisinesCounter.class, new HashMap<String, Object>());
    }

    @Test
    public void whenAddingCuisinesToCustomer_thenCounterUpdates() {

        /* create a customer */
        final Customer customer = new Customer(UUID.randomUUID(), "Hans");
        customerRepository.save((customer));

        /* create 2 distinct cuisines */
        final Cuisine indianCuisine = new Cuisine(UUID.randomUUID(), "Indian");
        cuisineRepository.save((indianCuisine));
        log.info("created Indian cusine: " + indianCuisine.getCuisineId());
        final Cuisine italianCuisine = new Cuisine(UUID.randomUUID(), "Italian");
        cuisineRepository.save((italianCuisine));
        log.info("created italian cusine: " + italianCuisine.getCuisineId());

        /*
          set the cuisines a customer is registered to
         */
        Set<Cuisine> allCuisinesForThisCustomer = new HashSet<>();
        allCuisinesForThisCustomer.add(italianCuisine);
        allCuisinesForThisCustomer.add(indianCuisine);

        /* save the cuisines set for this customer in the customer-cuisine table */
        final Customer_Cuisine customer_cuisine = new Customer_Cuisine(customer, allCuisinesForThisCustomer);
        customerCuisineRepository.save(customer_cuisine);

        List<Cuisine> cuisines = new ArrayList<>();
        Customer_Cuisine customerCuisineFromDb = customerCuisineRepository.findCustomer_CuisineByCustomerId(customer.getId());

        if (null != customerCuisineFromDb) {
            cuisines = customerCuisineFromDb.getCuisines().stream().peek(Objects :: requireNonNull).map(z -> cuisineRepository.findCuisineByCuisineID(z)).collect(Collectors.toList());
        }

        /* update number of cuisinesfor the customer in the counter table */
        customerCuisinesCounterRepository.updateNumberOfCuisinesForCustomer(customer.getId(), 2L);

        /* check if the counter was updtated */
        Long totalCuisinesForThisCustomer = customerCuisinesCounterRepository.getNumberOfCuisinesForOneCustomer(customer.getId());
        assertEquals(cuisines.size(), java.util.Optional.ofNullable(totalCuisinesForThisCustomer).get().longValue());
    }


    @After
    public void dropTables() {

        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_CUISINE_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_CUSTOMERS_COUNTER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_CUISINES_COUNTER_TABLE_NAME));

    }

}
