package de.quandoo.recruitment.registry.web.controllers;

import de.quandoo.recruitment.registry.api.CuisinesRegistryExtraOps;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.dto.CuisineDTO;
import de.quandoo.recruitment.registry.services.IRandomService;
import de.quandoo.recruitment.registry.utils.CuisineFormToCuisine;
import de.quandoo.recruitment.registry.utils.CuisineToCuisineForm;
import de.quandoo.recruitment.registry.web.CreateRandomCuisinesForm;
import de.quandoo.recruitment.registry.web.CuisineForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Set;
import java.util.UUID;

/**
 * Simple controller for testing
 */
@Controller
public class CuisineRegistryController {

    @Autowired
    IRandomService randomService;
    @Autowired
    CuisinesRegistryExtraOps cuisineRegistry;
    @Autowired
    private CuisineToCuisineForm cuisineToCuisineForm;
    @Autowired
    private CuisineFormToCuisine cuisineFormToCuisine;

    @RequestMapping(value = "/registerCuisine", method = RequestMethod.POST)
    public String register(@ModelAttribute Cuisine cuisine, Model model) {
        model.addAttribute("successMessage", "You have successfully registered for cuisine: " + cuisine.getName() + ".");

        return "cuisineRegistration.html";
    }

    @RequestMapping(value = "/registerCuisine", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("cuisine", new Cuisine());
        return "cuisineRegistration.html";
    }

    @RequestMapping(value = "/createCuisines", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("createCuisinesForm", new CreateRandomCuisinesForm());
        return "cuisineRandomform.html";
    }

    @RequestMapping(value = "/createCuisines", method = RequestMethod.POST)
    public String create(@Valid CreateRandomCuisinesForm cuisineForm, BindingResult bindingResult) {
        Set<Cuisine> created = cuisineRegistry.createRandomCuisinesWithRandomCustomers(cuisineForm.getHowManyCuisines(), cuisineForm.getHowManyCustomers());

        return "redirect:/list";
    }

    @RequestMapping("/")
    public String redirToList() {
        return "redirect:/list";
    }

    @RequestMapping("/top")
    public String listTopCuisines(Model model) {
        model.addAttribute("cuisines", cuisineRegistry.listAllCuisines());
        return "list";
    }

    @RequestMapping({"/list", "/cuisine"})
    public String listCuisines(Model model) {
        model.addAttribute("cuisines", cuisineRegistry.listAllCuisines());
        return "list";
    }

    @RequestMapping("/showCuisine/{id}")
    public String getCuisine(@PathVariable String id, Model model) {
        Long howManyCustomers = cuisineRegistry.getNumberOfCustomersForThisCuisine(UUID.fromString(id));
        model.addAttribute("cuisine", cuisineRegistry.findCuisineById(UUID.fromString(id)));
        model.addAttribute("numberOfCustomers", howManyCustomers);
        return "showCuisine";
    }

    @RequestMapping("edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        Cuisine cuisine = cuisineRegistry.findCuisineById(UUID.fromString(id));
        CuisineForm cuisineForm = cuisineToCuisineForm.convert(cuisine);
        model.addAttribute("cuisineForm", cuisineForm);
        return "cuisineform";
    }

    @RequestMapping("/new")
    public String newCuisine(Model model) {
        model.addAttribute("cuisineForm", new CuisineForm());
        return "cuisineform";
    }

    @RequestMapping(value = "/cuisine", method = RequestMethod.POST)
    public String saveOrUpdateAuto(@Valid CuisineForm cuisineForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || cuisineForm.getName().trim().equals("")) {
            return "cuisineform";
        }
        Cuisine notSaved = cuisineFormToCuisine.convert(cuisineForm);
        CuisineDTO cuisineRandom = randomService.getRandomCusisine();

        notSaved.setCuisineId(UUID.randomUUID());
        notSaved.setFoodTypes(cuisineRandom.getFoodTypes());
        notSaved.setDishTypes(cuisineRandom.getDishTypes());
        notSaved.setDishPrices(cuisineRandom.getDishPrices());

        Cuisine savedCuisine = cuisineRegistry.save(notSaved);


        return "redirect:/showCuisine/" + savedCuisine.getCuisineId();
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        cuisineRegistry.deleteCuisineById(UUID.fromString(id));
        return "redirect:/list";
    }

    @RequestMapping("/deleteAll")
    public String deleteAll() {
        cuisineRegistry.deleteAllData();
        return "redirect:/list";
    }
}