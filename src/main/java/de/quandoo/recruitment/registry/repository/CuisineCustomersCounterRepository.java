package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.CuisineCustomersCounter;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CuisineCustomersCounterRepository extends CassandraRepository<CuisineCustomersCounter, Long> {

    @Query("select number_of_registered_customers from cuisine_customers_counter where cuisine_id = ?0 ")
    Optional<Long> getNumberOfCustomersForOneCuisine(UUID cuisineId);

    /* @TODO  change it: cassandra does not allow sorting of COUNTER objects */
    @Query("select cuisine_id from cuisine_customers_counter ORDER BY number_of_registered_customers DESC limit ?0")
    List<UUID> getCuisineWithMostCustomers(int howMany);

    @Query("update cuisine_customers_counter SET number_of_registered_customers=number_of_registered_customers+1 WHERE cuisine_id = ?0 ")
    Object incrementNumberOfCustomersInCuisine(UUID cuisineId);

    @Query("update cuisine_customers_counter SET number_of_registered_customers=number_of_registered_customers+?1 WHERE cuisine_id = ?0 ")
    Object updateNumberOfCustomersInCuisine(UUID cuisineId, Long increment);

    @Query("update cuisine_customers_counter SET last_update=?1 WHERE cuisine_id = ?0 ")
    void setLastUpdateOn(UUID cuisineId, Date timestamp);
}
