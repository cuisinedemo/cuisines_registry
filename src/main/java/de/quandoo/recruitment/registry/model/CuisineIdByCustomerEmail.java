package de.quandoo.recruitment.registry.model;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

/**
 * Table for querying email from customers registered to a cuisine
 */
@Table
public class CuisineIdByCustomerEmail {
    @PrimaryKeyColumn(name = "customer_email", ordinal = 0, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private String customerEmail;

    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private UUID cuisineId;

}
