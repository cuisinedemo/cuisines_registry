package de.quandoo.recruitment.registry.model;

import com.datastax.driver.core.DataType;
import de.quandoo.recruitment.registry.model.dto.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

//import java.sql.Timestamp;

/**
 * Cassandra Entity class for a Cuisine object
 * <p>
 * Note that a quick and dirty approach, for production we would have
 * to split this class in several tables, as we need one table to query
 * each Set here ( foodTypes , etc. )
 */
@Table
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Cuisine extends BaseEntity {

    @Column
    Set<String> tags = new HashSet<>();
    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    @CassandraType(type = DataType.Name.UUID)
    private UUID cuisineId;
    @PrimaryKeyColumn(name = "name", ordinal = 1, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private String name;

    @Column("creation_date")
    private Date creationDate = new Date(System.currentTimeMillis());

    @Column("food_types")
    private Set<String> foodTypes = new HashSet<>();

    @Column("dish_types")
    private Set<String> dishTypes = new HashSet<>();

    @Column("dish_prices")
    private Map<String, Double> dishPrices = new HashMap<>();


    public Cuisine(final UUID cuisineId, final String name) {
        this.cuisineId = cuisineId;
        this.name = name;
    }

    public Cuisine(final UUID cuisineId, final String name, Set<String> tags) {
        this.cuisineId = cuisineId;
        this.name = name;
        this.tags.addAll(tags);
    }

    public Cuisine() {
    }

    public Date getCreationDate() {
        return creationDate;
    }


    public UUID getCuisineId() {
        return this.cuisineId;
    }

    public void setCuisineId(UUID cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getTags() {
        return this.tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public Set<String> getFoodTypes() {
        return this.foodTypes;
    }

    public void setFoodTypes(Set<String> foodTypes) {
        this.foodTypes = foodTypes;
    }

    public Set<String> getDishTypes() {
        return this.dishTypes;
    }

    public void setDishTypes(Set<String> dishTypes) {
        this.dishTypes = dishTypes;
    }

    public Map<String, Double> getDishPrices() {
        return this.dishPrices;
    }

    public void setDishPrices(Map<String, Double> dishPrices) {
        this.dishPrices = dishPrices;
    }
}
