package de.quandoo.recruitment.registry.web;

public class CreateRandomCuisinesForm {

    private int howManyCuisines;

    private int howManyCustomers;

    private String cuisineId;


    public int getHowManyCuisines() {
        return howManyCuisines;
    }

    public void setHowManyCuisines(int howManyCuisines) {
        this.howManyCuisines = howManyCuisines;
    }

    public int getHowManyCustomers() {
        return howManyCustomers;
    }

    public void setHowManyCustomers(int howManyCustomers) {
        this.howManyCustomers = howManyCustomers;
    }
}
