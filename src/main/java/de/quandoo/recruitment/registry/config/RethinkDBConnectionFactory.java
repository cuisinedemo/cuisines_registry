package de.quandoo.recruitment.registry.config;


import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;


/**
 * creates a connection to rethink db
 */
@PropertySource("classpath:rethinkdb.properties")
public class RethinkDBConnectionFactory {

    @Value("${rethinkdb.host}")
    String rethinkDbHost;

    @Value("${rethinkdb.port}")
    String rethinkDbPort;


    public RethinkDBConnectionFactory(String host) {
        this.rethinkDbHost = host;
    }

    public Connection createConnection() {
        try {
            RethinkDB.r.connection().hostname(rethinkDbHost).port(Integer.parseInt(rethinkDbPort));

            return RethinkDB.r.connection().hostname(rethinkDbHost).port(Integer.parseInt(rethinkDbPort)).connect();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}