package de.quandoo.recruitment.registry.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Utility class for rest operations
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CuisineDTO extends BaseEntity {

    private UUID cuisineId;
    private String name;
    @Builder.Default
    private Set<String> foodTypes = new HashSet<>();
    @Builder.Default
    private Set<String> dishTypes = new HashSet<>();
    @Builder.Default
    private Map<String, Double> dishPrices = new HashMap<>();

    public UUID getCuisineId() {
        return this.cuisineId;
    }

    public void setCuisineId(UUID cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getFoodTypes() {
        return this.foodTypes;
    }

    public void setFoodTypes(Set<String> foodTypes) {
        this.foodTypes = foodTypes;
    }

    public Set<String> getDishTypes() {
        return this.dishTypes;
    }

    public void setDishTypes(Set<String> dishTypes) {
        this.dishTypes = dishTypes;
    }

    public Map<String, Double> getDishPrices() {
        return this.dishPrices;
    }

    public void setDishPrices(Map<String, Double> dishPrices) {
        this.dishPrices = dishPrices;
    }

    public String toString() {
        return "CuisineDTO(cuisineId=" + this.getCuisineId() + ", name=" + this.getName() + ", foodTypes=" + this.getFoodTypes() + ", dishTypes=" + this.getDishTypes() + ", dishPrices=" + this.getDishPrices() + ")";
    }
}
