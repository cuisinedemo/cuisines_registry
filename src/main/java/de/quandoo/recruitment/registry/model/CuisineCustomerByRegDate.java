package de.quandoo.recruitment.registry.model;

import com.datastax.driver.core.DataType;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;
import java.util.UUID;

/**
 * Table that maps cuisine to customers. It contains all customers(ids) a
 * that are registered to a cusine.
 */
//@Builder
@Table
public class CuisineCustomerByRegDate {

    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private UUID cuisineId;

    @PrimaryKeyColumn(name = "customer_id", ordinal = 1, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private String customerId;

    @PrimaryKeyColumn(name = "registration_date", ordinal = 2, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    @CassandraType(type = DataType.Name.TIMESTAMP)
    private Date registrationDate;

    public CuisineCustomerByRegDate(final Cuisine cuisine, Customer customers) {
        this.cuisineId = cuisine.getCuisineId();
        this.customerId = cuisine.getName();
    }

    public CuisineCustomerByRegDate() {
    }

    public UUID getCuisineId() {
        return this.cuisineId;
    }

    public void setCuisineId(UUID cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Date getRegistrationDate() {
        return this.registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CuisineCustomerByRegDate)) return false;
        final CuisineCustomerByRegDate other = (CuisineCustomerByRegDate) o;
        if (!other.canEqual(this)) return false;
        final Object this$cuisineId = this.getCuisineId();
        final Object other$cuisineId = other.getCuisineId();
        if (this$cuisineId == null ? other$cuisineId != null : !this$cuisineId.equals(other$cuisineId)) return false;
        final Object this$customerId = this.getCustomerId();
        final Object other$customerId = other.getCustomerId();
        if (this$customerId == null ? other$customerId != null : !this$customerId.equals(other$customerId))
            return false;
        final Object this$registrationDate = this.getRegistrationDate();
        final Object other$registrationDate = other.getRegistrationDate();
        return this$registrationDate == null ? other$registrationDate == null : this$registrationDate.equals(other$registrationDate);
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $cuisineId = this.getCuisineId();
        result = result * PRIME + ($cuisineId == null ? 43 : $cuisineId.hashCode());
        final Object $customerId = this.getCustomerId();
        result = result * PRIME + ($customerId == null ? 43 : $customerId.hashCode());
        final Object $registrationDate = this.getRegistrationDate();
        result = result * PRIME + ($registrationDate == null ? 43 : $registrationDate.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof CuisineCustomerByRegDate;
    }

    public String toString() {
        return "CuisineCustomerByRegDate(cuisineId=" + this.getCuisineId() + ", customerId=" + this.getCustomerId() + ", registrationDate=" + this.getRegistrationDate() + ")";
    }
}
