package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.Cuisine_Customer;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CuisineCustomerRepository extends CassandraRepository<Cuisine_Customer, Long> {

    @Query("select * from cuisine_customer where cuisine_id = ?0 ")
    Cuisine_Customer findCuisine_CustomerByCuisineId(UUID cuisineId);

    @Query("select * from cuisine_customer where name = ?0 ")
    Iterable<Cuisine_Customer> findCuisine_CustomerByName(String name);

}
