package de.quandoo.recruitment.registry.services;

import com.namics.commons.random.RandomData;
import de.quandoo.recruitment.registry.model.dto.CuisineDTO;
import de.quandoo.recruitment.registry.model.dto.CustomerDTO;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Utility class to create random prices and fake customers/cuisines
 */
@Service
public class RandomServiceImpl implements IRandomService {

    final static List<String> places = Arrays.asList("Restaurant", "Imbiss", "Village", "In", "Spelunke", "Brasseria", "Tratoria", "Pizzeria", "Doenner");
    final static List<String> foodTypes = Arrays.asList("Indian", "Mexican", "Thai", "Vietnamese", "Lebanese", "Japanese", "German", "French", "Italian", "Spanish");
    final static List<String> dishTypes = Arrays.asList("Menu", "Lunch", "Business", "Take-away", "Family Menu", "Kid's Menu", "Big Portion", "Small Portion");
    final static List<String> dishNames = Arrays.asList("Menu nr. 1", "Veggie Lunch ", "Barbecue 400g", "Cheeseburger Hawai Style", "Rice Mandala ", "Kid's Menu(Gluten free)", "Pork Toasted", "Sushi (10 Pieces)", "Kala Manala", "Ramen", "Cold Baskwts", "Kartofel mit Schwein", "Pommes", "Fried Rice", "Fried Chicken", "4 Burritos", "Doenner", "Le Pastillete au Fondue", "Kotleta", "Gulash Knedlik", "Wow Wu", "Chi Vion Nai (big portion)", "Menu nr. 2");


    @Override
    public double getRandomPrice() {
        return (Math.ceil(RandomData.randomDouble(1.00, 25.00) * 2) / 2);
    }

    @Override
    public CustomerDTO getRandomCustomer() {
        return RandomData.random(CustomerDTO.class);
    }

    @Override
    public CuisineDTO getRandomCusisine() {
        CuisineDTO cuisineDTO = new CuisineDTO();
        cuisineDTO.setCuisineId(UUID.randomUUID());
        int randomNum = ThreadLocalRandom.current().nextInt(0, places.size());
        cuisineDTO.setName(RandomData.firstname() + "'s " + places.get(randomNum));
        cuisineDTO.setDishTypes(createSet(2, dishTypes));
        cuisineDTO.setFoodTypes(createSet(5, foodTypes));
        cuisineDTO.setDishPrices(createPricesMap(5, dishNames));
        return cuisineDTO;
    }

    @Override
    public UUID getRandomId() {
        return UUID.randomUUID();
    }

    public Set<String> createSet(int numberOfItems, List<String> category) {
        Random random = new Random();
        Set<String> itemsSet = random.
                ints(numberOfItems, 0, category.size()).
                mapToObj(i -> category.get(i)).
                collect(Collectors.toSet());
        return itemsSet;
    }

    public Map<String, Double> createPricesMap(int numberOfItems, List<String> category) {
        Map<String, Double> pricesMap = new HashMap<>();
        createSet(5, dishNames).stream().collect(Collectors.toList()).forEach(d -> {
            pricesMap.put(d, this.getRandomPrice());
        });
        return pricesMap;
    }
}
