package de.quandoo.recruitment.registry.repositories;

import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineCustomersCounter;
import de.quandoo.recruitment.registry.model.Cuisine_Customer;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.repository.CuisineCustomerRepository;
import de.quandoo.recruitment.registry.repository.CuisineCustomersCounterRepository;
import de.quandoo.recruitment.registry.repository.CuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * CuisineCustomer table maps cuisines to customers
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraTestConfig.class)
@EnableCassandraRepositories("de.quandoo.recruitment.registry.repository")
public class CuisineCustomersRepositoryIntegrationTest extends AbstractCuisineRepoTest {


    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String CUISINE_TABLE_NAME = "cuisine";
    public static final String CUISINE_CUSTOMER_TABLE_NAME = "cuisine_customer";
    public static final String CUISINE_CUSTOMERS_COUNTER_TABLE_NAME = "cuisine_customers_counter";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CuisineRepository cuisineRepository;

    @Autowired
    private CuisineCustomerRepository cuisineCustomerRepository;

    @Autowired
    private CuisineCustomersCounterRepository cuisineCustomersCounterRepository;

    @Autowired
    private CassandraAdminOperations adminTemplate;


    @AfterClass
    public static void stopCassandraEmbedded() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

    @Before
    public void createTable() {

        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_TABLE_NAME), Customer.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_TABLE_NAME), Cuisine.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_CUSTOMER_TABLE_NAME), Cuisine_Customer.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_CUSTOMERS_COUNTER_TABLE_NAME), CuisineCustomersCounter.class, new HashMap<String, Object>());

    }


    @Test
    public void whenAddingCustomersToCuisine_thenCounterUpdates() {

        /* create a cuisine */
        final Cuisine mexicanCuisine = new Cuisine(UUID.randomUUID(), "Mexican");
        cuisineRepository.save(mexicanCuisine);


        /* create 3 distinct customers */
        final Customer hans = new Customer(UUID.randomUUID(), "Hans");
        customerRepository.save(hans);
        log.info("created customer with id  " + hans.getId());
        final Customer magdalena = new Customer(UUID.randomUUID(), "Magdalena");
        customerRepository.save((magdalena));
        log.info("created customer with id  " + magdalena.getId());
        final Customer drKrueger = new Customer(UUID.randomUUID(), "Dr.Krueger");
        customerRepository.save(drKrueger);
        log.info("created customer with id  " + drKrueger.getId());


        /*
          set the cuisines a customer is registered to
         */
        Set<Customer> allCustomersForThisCuisine = new HashSet<>();
        allCustomersForThisCuisine.add(hans);
        allCustomersForThisCuisine.add(magdalena);
        allCustomersForThisCuisine.add(drKrueger);


        /* save the customers set for this cuisine in the  cuisine-customer table */
        final Cuisine_Customer cuisine_Customer = new Cuisine_Customer(mexicanCuisine, allCustomersForThisCuisine);
        cuisineCustomerRepository.save(cuisine_Customer);

        /* update number of customers for the cuisine in the counter table */
        cuisineCustomersCounterRepository.updateNumberOfCustomersInCuisine(mexicanCuisine.getCuisineId(), 2L);

        /* check if the cuisine counter was updtated */
        Optional<Long> totalCustomersForThisCuisine = cuisineCustomersCounterRepository.getNumberOfCustomersForOneCuisine(mexicanCuisine.getCuisineId());
        // Iterator numberOfCustomersFromDB = totalCustomersForThisCuisine. ;
        assertEquals(2L, totalCustomersForThisCuisine.get().longValue());
    }


    @After
    public void dropTables() {

        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_CUSTOMER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_CUSTOMERS_COUNTER_TABLE_NAME));

    }

}
