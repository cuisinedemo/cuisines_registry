package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.Customer;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CustomerRepository extends CassandraRepository<Customer, Long> {

    @Query("select * from customer where id = ?0 ")
    Customer findCustomerById(UUID id);

    @Query("select * from customer where name = ?0 ")
    Iterable<Customer> findCustomerByName(String name);

    @Query("INSERT INTO customer (id, name, email) " + "  values ( ?0 ,  ?1 ,   ?2  ) IF NOT EXISTS")
    Customer saveCustomerIfNotExists(UUID id, String name, String email);

    @Query("UPDATE customer  SET  name= ?1 ,  email= ?2 " + " where id= ?0 ")
    Customer updateCustomer(UUID id, String name, String email);

    @Query("delete  from customer where id = ?0 ")
    Customer deleteCustomerById(UUID id);
}
