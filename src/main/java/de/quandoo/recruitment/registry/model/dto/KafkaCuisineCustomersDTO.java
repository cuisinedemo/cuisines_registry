package de.quandoo.recruitment.registry.model.dto;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * POJO class used for publishing cuisines with registered customers to Kafka.
 * Each time a customer registers to a cuisine the app publishes a message to
 * Kafka.
 */
@Builder
@JsonAutoDetect
@NoArgsConstructor
@AllArgsConstructor
public class KafkaCuisineCustomersDTO extends BaseEntity {

    Date registeredToCuisineOnDate;
    private String cuisineId;
    private String customerId;

    public Date getRegisteredToCuisineOnDate() {
        return registeredToCuisineOnDate;
    }

    public void setRegisteredToCuisineOnDate(Date registeredToCuisineOnDate) {
        this.registeredToCuisineOnDate = registeredToCuisineOnDate;
    }

    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
