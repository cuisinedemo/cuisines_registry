package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.CuisineCustomersCounter;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CustomerCuisinesCounterRepository extends CassandraRepository<CuisineCustomersCounter, Long> {

    @Query("select number_of_cuisines_for_customer from customer_cuisines_counter where customer_id = ?0 ")
    Long getNumberOfCuisinesForOneCustomer(UUID customerId);

    @Query("select customer_id from customer_cuisines_counter ORDER BY customer_cuisines_counter DESCENDING")
    Iterable<Integer> getCustomersWithMostCuisines();

    @Query("update customer_cuisines_counter SET number_of_cuisines_for_customer=number_of_cuisines_for_customer+1 WHERE customer_id = ?0 ")
    Object incrementNumberOfCuisinesForCustomer(UUID customerId);

    @Query("update customer_cuisines_counter SET number_of_cuisines_for_customer=number_of_cuisines_for_customer +?1  WHERE customer_id = ?0 ")
    Object updateNumberOfCuisinesForCustomer(UUID customerId, Long increment);
}
