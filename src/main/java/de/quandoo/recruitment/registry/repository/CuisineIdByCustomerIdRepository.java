package de.quandoo.recruitment.registry.repository;

import de.quandoo.recruitment.registry.model.Cuisine;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CuisineIdByCustomerIdRepository extends CassandraRepository<Cuisine, Long> {

    @Query("select cuisine_id from cuisine_id_by_customer_id where customer_id = ?0 ORDER BY cuisine:id ASC ")
    Iterable<Cuisine> findCuisineByCustomerId(UUID customerId);

}



