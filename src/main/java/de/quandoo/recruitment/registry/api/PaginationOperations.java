package de.quandoo.recruitment.registry.api;

import de.quandoo.recruitment.registry.model.Cuisine;
import org.springframework.data.domain.Slice;

/**
 * for providing pagination for cassandra.
 * Not really implemented, just started...
 */
public interface PaginationOperations {

    Slice<Cuisine> findPaginated(int page, int size);

}
