package de.quandoo.recruitment.registry.web;


import org.springframework.stereotype.Component;

import java.util.UUID;


@Component
public class CuisineForm {

    private UUID cuisineId;
    private String name;


    public UUID getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(UUID cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
