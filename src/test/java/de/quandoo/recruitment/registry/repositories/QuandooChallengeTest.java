package de.quandoo.recruitment.registry.repositories;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.config.RethinkDBConnectionFactory;
import de.quandoo.recruitment.registry.config.RethinkDbConfig;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineCustomersCounter;
import de.quandoo.recruitment.registry.model.Cuisine_Customer;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.CustomerCuisinesCounter;
import de.quandoo.recruitment.registry.model.Customer_Cuisine;
import de.quandoo.recruitment.registry.model.dto.CuisineCountDTO;
import de.quandoo.recruitment.registry.repository.CuisineCustomerRepository;
import de.quandoo.recruitment.registry.repository.CuisineCustomersCounterRepository;
import de.quandoo.recruitment.registry.repository.CuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerCuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerCuisinesCounterRepository;
import de.quandoo.recruitment.registry.repository.CustomerRepository;
import de.quandoo.recruitment.registry.services.IRandomService;
import de.quandoo.recruitment.registry.utils.CuisinesCreator;
import lombok.extern.slf4j.Slf4j;
import org.apache.cassandra.exceptions.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;


/**
 * The actual answer to the Quandoo recruitment challenge:
 * <p>
 * 1) Customers should follow more than one Cuisine
 * <p>
 * 2) Retrieve the Cuisine Objects with the most Customers
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CassandraTestConfig.class, RethinkDbConfig.class, CuisinesCreator.class})
@EnableCassandraRepositories("de.quandoo.recruitment.registry.repository")
public class QuandooChallengeTest extends AbstractCuisineRepoTest {

    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String CUISINE_TABLE_NAME = "cuisine";
    public static final String CUISINE_CUSTOMER_TABLE_NAME = "cuisine_customer";
    public static final String CUISINE_CUSTOMERS_COUNTER_TABLE_NAME = "cuisine_customers_counter";
    public static final String CUSTOMER_CUISINES_COUNTER_TABLE_NAME = "customer_cuisines_counter";
    public static final String CUSTOMER_CUISINE_TABLE_NAME = "customer_cuisine";

    public static final RethinkDB r = RethinkDB.r;

    @Autowired
    public RethinkDBConnectionFactory connectionFactory;

    @Autowired
    public CuisinesCreator cuisinesCreator;

    @Value("${rethinkdb.database}")
    String rethinkDbDatabase;


    @Autowired
    IRandomService randomService;
    Connection conn = null;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CuisineRepository cuisineRepository;
    @Autowired
    private CustomerCuisineRepository customerCuisineRepository;
    @Autowired
    private CuisineCustomerRepository cuisineCustomerRepository;
    @Autowired
    private CuisineCustomersCounterRepository cuisineCustomersCounterRepository;
    @Autowired
    private CustomerCuisinesCounterRepository customerCuisinesCounterRepository;
    @Autowired
    private CassandraAdminOperations adminTemplate;

    /**
     * Create cassandra tables needed for the test
     *
     * @throws ConfigurationException
     */
    @Before
    public void createTable() throws ConfigurationException {

        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_TABLE_NAME), Customer.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_TABLE_NAME), Cuisine.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_CUSTOMER_TABLE_NAME), Cuisine_Customer.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_CUISINE_TABLE_NAME), Customer_Cuisine.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_CUISINES_COUNTER_TABLE_NAME), CustomerCuisinesCounter.class, new HashMap<String, Object>());
        adminTemplate.createTable(true, CqlIdentifier.of(CUISINE_CUSTOMERS_COUNTER_TABLE_NAME), CuisineCustomersCounter.class, new HashMap<String, Object>());

        conn = connectionFactory.createConnection();

    }

    /**
     * Recruitment Challenge nr. 1:  Customer can follow 'n' Cuisines
     * <p>
     * This method creates a set of customers, iterates through this set and for each element
     * adds a set of cuisine objects to it, saving the cuisine and customer objets. Every  time
     * a customer is registered by a cuisine a counter of the customers for that cuisine is manually update
     */
    @Test
    public void customerShouldFollowMoreThanOneCuisineTest() {

        /* create 3 sets of customers of distinct sizes */
        Set<Customer> fakeCustomers = cuisinesCreator.createRandomCustomers(3);
        customerRepository.saveAll(fakeCustomers);


        int[] howManyCuisinesToCreate = new int[]{5, 10, 15};
        final AtomicInteger runCount = new AtomicInteger();
        runCount.set(0);


        /* for each cuisine create n customers and save in both customer and cuisine-customer tables */
        fakeCustomers.forEach(tempCustomer -> {
            Set<Cuisine> tempCuisines = cuisinesCreator.createRandomCuisines(howManyCuisinesToCreate[runCount.getAndIncrement()]);
            cuisineRepository.saveAll(tempCuisines);
            Customer_Cuisine customer_cuisine = new Customer_Cuisine(tempCustomer, tempCuisines);
            customerCuisineRepository.save(customer_cuisine);
            customerCuisinesCounterRepository.updateNumberOfCuisinesForCustomer(tempCustomer.getId(), (long) tempCuisines.size());
            assertEquals(tempCuisines.size(), customerCuisineRepository.findCustomer_CuisineByCustomerId(tempCustomer.getId()).getCuisines().size());
        });
    }


    /**
     * Recruitment Challenge nr. 2: get the Cuisine with the most Customers
     */
    @Test
    public void retrieveCuisinesWithMostCustomersTest() {

        int n = 10; // how many cusines to return from sorted list
        ObjectMapper mapper = new ObjectMapper();

        /* create 10 cuisines */
        Set<Cuisine> fakeCuisines = cuisinesCreator.createRandomCuisines(10);
        cuisineRepository.saveAll(fakeCuisines);
        // int[] howManyCustomersToCreate = new int[]{5, 10, 15};
        final AtomicInteger customersArrayCount = new AtomicInteger();
        customersArrayCount.set(0);

        /* for each cuisine create n customers and save in both customer and cuisine-customer tables */
        fakeCuisines.forEach(tempCuisine -> {

            int howManyCustomersToCreate = ThreadLocalRandom.current().nextInt(80);
            Set<Customer> fakeCustomers = cuisinesCreator.createRandomCustomers(howManyCustomersToCreate);
            customerRepository.saveAll(fakeCustomers);
            Cuisine_Customer cuisine_customer = new Cuisine_Customer(tempCuisine, fakeCustomers);
            cuisineCustomerRepository.save(cuisine_customer);
            cuisineCustomersCounterRepository.updateNumberOfCustomersInCuisine(tempCuisine.getCuisineId(), (long) fakeCustomers.size());
            /* save the cuisine-id and count to the rethinkdb */
            r.db(rethinkDbDatabase).table("customers_in_cuisine").insert(r.hashMap("cuisine_id", tempCuisine.getCuisineId().toString()).with("count", fakeCustomers.size())).run(conn);
            log.debug("Saving customer counts in cuisine tabe ( rethink db )");
        });


        List<String> mostPopularCuisines = new ArrayList<>();

        /* get a sorted list of the n most popular cuisines */
        List<Object> sortedCusineDtoFromRethink = r.db(rethinkDbDatabase).table("customers_in_cuisine").orderBy().optArg("index", r.desc("count")).map(x -> x.toJsonString()).coerceTo("array").limit(10).run(conn, CuisineCountDTO.class);

        log.info("Top " + n + " cuisines: ");

        /* get a dto json object from rethink db */
        sortedCusineDtoFromRethink.stream().forEach(row -> {
            try {
                CuisineCountDTO cuisineCountDTO = mapper.readValue((String) row, CuisineCountDTO.class);
                log.info(cuisineCountDTO.toString());
                mostPopularCuisines.add(cuisineCountDTO.getCuisine_id());
            } catch (IOException e) {
                log.error(e.toString());
            }
        });
        /* as we have only the DTO from Cuisines, get the actual object from cassandra db */
        List<Long> cuisines = mostPopularCuisines.stream().peek(Objects :: requireNonNull).map(id -> cuisineCustomersCounterRepository.getNumberOfCustomersForOneCuisine(UUID.fromString(id)).get()).collect(Collectors.toList());
        assertThat(cuisines).isSortedAccordingTo(Comparator.reverseOrder());
    }

    /**
     * clean up ...
     */
    @After
    public void dropTables() {

        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_CUSTOMER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUISINE_CUSTOMERS_COUNTER_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_CUISINE_TABLE_NAME));
        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_CUISINES_COUNTER_TABLE_NAME));

        conn.close();

    }


}
