package de.quandoo.recruitment.registry.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import de.quandoo.recruitment.registry.api.CuisinesRegistryExtraOps;
import de.quandoo.recruitment.registry.api.PaginationOperations;
import de.quandoo.recruitment.registry.config.RethinkDBConnectionFactory;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Cuisine_Customer;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.Customer_Cuisine;
import de.quandoo.recruitment.registry.model.dto.CuisineCountDTO;
import de.quandoo.recruitment.registry.model.dto.KafkaCuisineCustomersDTO;
import de.quandoo.recruitment.registry.repository.CuisineCustomerRepository;
import de.quandoo.recruitment.registry.repository.CuisineCustomersCounterRepository;
import de.quandoo.recruitment.registry.repository.CuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerCuisineRepository;
import de.quandoo.recruitment.registry.repository.CustomerCuisinesCounterRepository;
import de.quandoo.recruitment.registry.repository.CustomerRepository;
import de.quandoo.recruitment.registry.utils.CuisinesCreator;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@SuppressWarnings("Duplicates")
@Service
@PropertySource({"classpath:rethinkdb.properties", "classpath:application.properties"})
public class CuisinesRegistryImp implements CuisinesRegistryExtraOps, PaginationOperations {

    public static final RethinkDB r = RethinkDB.r;

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CuisinesRegistryImp.class);

    final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public RethinkDBConnectionFactory connectionFactory;

    Connection conn = null;

    @Value("${rethinkdb.database}")
    String rethinkDbDatabase;
    @Value("${rethinkdb.table.name}")
    String rethinkDbTableName;

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CuisineRepository cuisineRepository;
    @Autowired
    private CustomerCuisineRepository customerCuisineRepository;
    @Autowired
    private CuisineCustomerRepository cuisineCustomerRepository;
    @Autowired
    private CustomerCuisinesCounterRepository customerCuisinesCounterRepository;
    @Autowired
    private CuisineCustomersCounterRepository cuisineCustomersCounterRepository;


    @Autowired
    private CuisinesCreator cuisinesCreator;

    @Autowired
    private KafkaTemplate<String, KafkaCuisineCustomersDTO> kafkaTemplate;

    @Value("${kafka.topic.name}")
    private String topic;


    @PostConstruct
    void initConnection() {
        if (null == conn) conn = connectionFactory.createConnection();
    }

    @Override
    public void register(Customer customer, Cuisine cuisine) {

        /* save the customer */
        customerRepository.saveCustomerIfNotExists(customer.getId(), customer.getName(), customer.getEmail());

        /* save the cuisine  */
        cuisineRepository.saveCuisineIfNotExists(cuisine.getCuisineId(), cuisine.getName());

        /* save the customer for this cuisine in the cuisine-customer  table */
        final Cuisine_Customer cuisine_customer = new Cuisine_Customer(cuisine, ImmutableSet.of(customer));
        cuisineCustomerRepository.save(cuisine_customer);

        /* update number of customers for the cuisine in the  cuisine counter table */
        cuisineCustomersCounterRepository.incrementNumberOfCustomersInCuisine(cuisine.getCuisineId());
        cuisineCustomersCounterRepository.setLastUpdateOn(cuisine.getCuisineId(), new Timestamp(System.currentTimeMillis()));

        /* publish to kafka */
        KafkaCuisineCustomersDTO temp = new KafkaCuisineCustomersDTO();
        temp.setCuisineId(cuisine.getCuisineId().toString());
        temp.setCustomerId(customer.getId().toString());
        temp.setRegisteredToCuisineOnDate(new Date());
        publishToKafka(temp);
    }

    /**
     * returns all the cuisines a customer is registered to
     *
     * @param customer the customer object
     * @return a list of cuisines
     */
    @Override
    public List<Cuisine> customerCuisines(Customer customer) {
        List<Cuisine> cuisines = new ArrayList<>();

        Customer_Cuisine customerCuisineFromDb = customerCuisineRepository.findCustomer_CuisineByCustomerId(customer.getId());

        if (null != customerCuisineFromDb) {
            cuisines = customerCuisineFromDb.getCuisines().stream().peek(Objects :: requireNonNull).map(z -> cuisineRepository.findCuisineByCuisineID(z)).collect(Collectors.toList());
        }
        return cuisines;
    }

    /**
     * Gets the 'n'  most popular cuisines ( with the most customers registered to )
     *
     * @param n : how many cuisines in descending order we want to fetch
     * @return the list in descending order of the most popular cuisines
     */
    @Override
    public List<Cuisine> topCuisines(int n) {
        List<String> mostPopularCuisines = new ArrayList<>();
        List<Object> sortedCusineDtoFromRethink = r.db(rethinkDbDatabase).table("customers_in_cuisine").orderBy().optArg("index", r.desc("count")).map(x -> x.toJsonString()).coerceTo("array").limit(n).run(conn, CuisineCountDTO.class);
        log.info("Top " + n + " cuisines: ");
        /* get a dto json object from rethink db */
        sortedCusineDtoFromRethink.stream().forEach(row -> {
            try {
                CuisineCountDTO cuisineCountDTO = mapper.readValue((String) row, CuisineCountDTO.class);
                log.info(cuisineCountDTO.toString());
                mostPopularCuisines.add(cuisineCountDTO.getCuisine_id());
            } catch (IOException e) {
                log.error(e.toString());
            }
        });
        /* as we have only the DTO from Cuisines, get the actual object from cassandra db */
        List<Cuisine> cuisines = mostPopularCuisines.stream().peek(Objects :: requireNonNull).map(id -> cuisineRepository.findCuisineByCuisineID(UUID.fromString(id))).collect(Collectors.toList());
        return cuisines;
    }


    @Override
    public List<Customer> cuisineCustomers(Cuisine cuisine) {

        List<Customer> customers = new ArrayList<>();
        Cuisine_Customer cuisine_customerFromDb = cuisineCustomerRepository.findCuisine_CustomerByCuisineId(cuisine.getCuisineId());
        if (null != cuisine_customerFromDb) {
            customers = cuisine_customerFromDb.getCustomers().stream().peek(Objects :: requireNonNull).map(z -> customerRepository.findCustomerById(z)).collect(Collectors.toList());
        }
        return customers;
    }


    @Override
    public List<Cuisine> listAllCuisines() {
        return cuisineRepository.findAll();
    }

    @Override
    public Cuisine findCuisineById(UUID cuisineId) {
        return cuisineRepository.findCuisineByCuisineID(cuisineId);
    }


    @Override
    public Cuisine save(Cuisine cuisine) {
        cuisineCustomersCounterRepository.incrementNumberOfCustomersInCuisine(cuisine.getCuisineId());
        return cuisineRepository.save(cuisine);
    }

    @Override
    public Long getNumberOfCustomersForThisCuisine(UUID cuisineId) {
        return cuisineCustomersCounterRepository.getNumberOfCustomersForOneCuisine(cuisineId).get();
    }

    @Override
    public void deleteAllCuisines() {
        cuisineCustomersCounterRepository.deleteAll();
        cuisineRepository.deleteAll();
        customerCuisinesCounterRepository.deleteAll();
        r.db(rethinkDbDatabase).tableDrop(rethinkDbTableName).run(conn);
        r.db(rethinkDbDatabase).tableCreate(rethinkDbTableName).run(conn);
        r.db(rethinkDbDatabase).table(rethinkDbTableName).indexCreate("count").run(conn);

    }

    @Override
    public void deleteAllData() {
        cuisineCustomersCounterRepository.deleteAll();
        cuisineRepository.deleteAll();
        customerCuisineRepository.deleteAll();
        customerCuisinesCounterRepository.deleteAll();
        customerRepository.deleteAll();
        r.db(rethinkDbDatabase).tableDrop(rethinkDbTableName).run(conn);
        r.db(rethinkDbDatabase).tableCreate(rethinkDbTableName).run(conn);
        r.db(rethinkDbDatabase).table(rethinkDbTableName).indexCreate("count").run(conn);

    }

    @KafkaListener(topics = "${kafka.topic.name}", containerFactory = "cuisineCustomersKafkaListenerContainerFactory", groupId = "kafka")
    public void cusineCustomerDtoListener(KafkaCuisineCustomersDTO kafkaCuisineCustomersDTO) {
        log.info("Received CuisineCustomer Count message: " + kafkaCuisineCustomersDTO);

    }

    @Override
    public String publishToKafka(KafkaCuisineCustomersDTO kafkaCuisineCustomersDTO) {
        kafkaTemplate.setDefaultTopic(topic);
        kafkaTemplate.send(topic, kafkaCuisineCustomersDTO);
        kafkaTemplate.flush();
        return kafkaCuisineCustomersDTO.toString();
    }


    @Override
    public void deleteCuisineById(UUID cuisineId) {
        cuisineRepository.deleteCuisineByCuisineId(cuisineId);
    }


    /**
     * utility method to create a bunch of random cuisines with random customer registered to it
     * The number of customers per cuisine will be random, you can set the maximum though.
     *
     * @param howManyCuisines
     * @param maxCustomersPerCuisine
     * @return
     */
    public Set<Cuisine> createRandomCuisinesWithRandomCustomers(int howManyCuisines, int maxCustomersPerCuisine) {

        /* create n cuisines */
        Set<Cuisine> fakeCuisines = cuisinesCreator.createRandomCuisines(howManyCuisines);
        cuisineRepository.saveAll(fakeCuisines);
        final AtomicInteger customersArrayCount = new AtomicInteger();
        customersArrayCount.set(0);

        /* for each cuisine create n customers and save in both customer and cuisine-customer tables */
        fakeCuisines.forEach(tempCuisine -> {

            int howManyCustomersToCreate = ThreadLocalRandom.current().nextInt(maxCustomersPerCuisine);
            Set<Customer> fakeCustomers = cuisinesCreator.createRandomCustomers(howManyCustomersToCreate);
            customerRepository.saveAll(fakeCustomers);

            /* publish cuisine-customers registration to Kafka */
            fakeCustomers.forEach(fakeCustomer -> {
                KafkaCuisineCustomersDTO forKafka = KafkaCuisineCustomersDTO.builder().cuisineId(tempCuisine.getCuisineId().toString()).customerId(fakeCustomer.getId().toString()).registeredToCuisineOnDate(new Date()).build();
                publishToKafka(forKafka);

            });

            /* we have to save it to cassandra to */
            Cuisine_Customer cuisine_customer = new Cuisine_Customer(tempCuisine, fakeCustomers);
            cuisineCustomerRepository.save(cuisine_customer);
            cuisineCustomersCounterRepository.updateNumberOfCustomersInCuisine(tempCuisine.getCuisineId(), (long) fakeCustomers.size());

            /* save the cuisine-id and count to the rethinkdb for sorting popular cuisines later  */
            r.db(rethinkDbDatabase).table("customers_in_cuisine").insert(r.hashMap("cuisine_id", tempCuisine.getCuisineId().toString()).with("count", fakeCustomers.size())).run(conn);
            log.info("Saving customer counts in cuisine table ( rethink db )");
        });

        return fakeCuisines;
    }


    //######## from here:  needed to delombok, as gradle can have problems in different ide's

    public Connection getConn() {
        return this.conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public String getRethinkDbDatabase() {
        return this.rethinkDbDatabase;
    }

    public void setRethinkDbDatabase(String rethinkDbDatabase) {
        this.rethinkDbDatabase = rethinkDbDatabase;
    }

    public CustomerRepository getCustomerRepository() {
        return this.customerRepository;
    }

    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public CuisineRepository getCuisineRepository() {
        return this.cuisineRepository;
    }

    public void setCuisineRepository(CuisineRepository cuisineRepository) {
        this.cuisineRepository = cuisineRepository;
    }

    public CustomerCuisineRepository getCustomerCuisineRepository() {
        return this.customerCuisineRepository;
    }

    public void setCustomerCuisineRepository(CustomerCuisineRepository customerCuisineRepository) {
        this.customerCuisineRepository = customerCuisineRepository;
    }

    public CuisineCustomerRepository getCuisineCustomerRepository() {
        return this.cuisineCustomerRepository;
    }

    public void setCuisineCustomerRepository(CuisineCustomerRepository cuisineCustomerRepository) {
        this.cuisineCustomerRepository = cuisineCustomerRepository;
    }

    public CustomerCuisinesCounterRepository getCustomerCuisinesCounterRepository() {
        return this.customerCuisinesCounterRepository;
    }

    public void setCustomerCuisinesCounterRepository(CustomerCuisinesCounterRepository customerCuisinesCounterRepository) {
        this.customerCuisinesCounterRepository = customerCuisinesCounterRepository;
    }

    public CuisineCustomersCounterRepository getCuisineCustomersCounterRepository() {
        return this.cuisineCustomersCounterRepository;
    }

    public void setCuisineCustomersCounterRepository(CuisineCustomersCounterRepository cuisineCustomersCounterRepository) {
        this.cuisineCustomersCounterRepository = cuisineCustomersCounterRepository;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CuisinesRegistryImp)) return false;
        final CuisinesRegistryImp other = (CuisinesRegistryImp) o;
        if (!other.canEqual(this)) return false;
        final Object this$conn = this.getConn();
        final Object other$conn = other.getConn();
        if (this$conn == null ? other$conn != null : !this$conn.equals(other$conn)) return false;

        final Object this$rethinkDbDatabase = this.getRethinkDbDatabase();
        final Object other$rethinkDbDatabase = other.getRethinkDbDatabase();
        if (this$rethinkDbDatabase == null ? other$rethinkDbDatabase != null : !this$rethinkDbDatabase.equals(other$rethinkDbDatabase))
            return false;
        final Object this$customerRepository = this.getCustomerRepository();
        final Object other$customerRepository = other.getCustomerRepository();
        if (this$customerRepository == null ? other$customerRepository != null : !this$customerRepository.equals(other$customerRepository))
            return false;
        final Object this$cuisineRepository = this.getCuisineRepository();
        final Object other$cuisineRepository = other.getCuisineRepository();
        if (this$cuisineRepository == null ? other$cuisineRepository != null : !this$cuisineRepository.equals(other$cuisineRepository))
            return false;
        final Object this$customerCuisineRepository = this.getCustomerCuisineRepository();
        final Object other$customerCuisineRepository = other.getCustomerCuisineRepository();
        if (this$customerCuisineRepository == null ? other$customerCuisineRepository != null : !this$customerCuisineRepository.equals(other$customerCuisineRepository))
            return false;
        final Object this$cuisineCustomerRepository = this.getCuisineCustomerRepository();
        final Object other$cuisineCustomerRepository = other.getCuisineCustomerRepository();
        if (this$cuisineCustomerRepository == null ? other$cuisineCustomerRepository != null : !this$cuisineCustomerRepository.equals(other$cuisineCustomerRepository))
            return false;
        final Object this$customerCuisinesCounterRepository = this.getCustomerCuisinesCounterRepository();
        final Object other$customerCuisinesCounterRepository = other.getCustomerCuisinesCounterRepository();
        if (this$customerCuisinesCounterRepository == null ? other$customerCuisinesCounterRepository != null : !this$customerCuisinesCounterRepository.equals(other$customerCuisinesCounterRepository))
            return false;
        final Object this$cuisineCustomersCounterRepository = this.getCuisineCustomersCounterRepository();
        final Object other$cuisineCustomersCounterRepository = other.getCuisineCustomersCounterRepository();
        return this$cuisineCustomersCounterRepository == null ? other$cuisineCustomersCounterRepository == null : this$cuisineCustomersCounterRepository.equals(other$cuisineCustomersCounterRepository);
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $conn = this.getConn();
        result = result * PRIME + ($conn == null ? 43 : $conn.hashCode());
        final Object $rethinkDbDatabase = this.getRethinkDbDatabase();
        result = result * PRIME + ($rethinkDbDatabase == null ? 43 : $rethinkDbDatabase.hashCode());
        final Object $customerRepository = this.getCustomerRepository();
        result = result * PRIME + ($customerRepository == null ? 43 : $customerRepository.hashCode());
        final Object $cuisineRepository = this.getCuisineRepository();
        result = result * PRIME + ($cuisineRepository == null ? 43 : $cuisineRepository.hashCode());
        final Object $customerCuisineRepository = this.getCustomerCuisineRepository();
        result = result * PRIME + ($customerCuisineRepository == null ? 43 : $customerCuisineRepository.hashCode());
        final Object $cuisineCustomerRepository = this.getCuisineCustomerRepository();
        result = result * PRIME + ($cuisineCustomerRepository == null ? 43 : $cuisineCustomerRepository.hashCode());
        final Object $customerCuisinesCounterRepository = this.getCustomerCuisinesCounterRepository();
        result = result * PRIME + ($customerCuisinesCounterRepository == null ? 43 : $customerCuisinesCounterRepository.hashCode());
        final Object $cuisineCustomersCounterRepository = this.getCuisineCustomersCounterRepository();
        result = result * PRIME + ($cuisineCustomersCounterRepository == null ? 43 : $cuisineCustomersCounterRepository.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof CuisinesRegistryImp;
    }

    public String toString() {
        return "CuisinesRegistryImp(conn=" + this.getConn() + ", rethinkDbDatabase=" + this.getRethinkDbDatabase() + ", customerRepository=" + this.getCustomerRepository() + ", cuisineRepository=" + this.getCuisineRepository() + ", customerCuisineRepository=" + this.getCustomerCuisineRepository() + ", cuisineCustomerRepository=" + this.getCuisineCustomerRepository() + ", customerCuisinesCounterRepository=" + this.getCustomerCuisinesCounterRepository() + ", cuisineCustomersCounterRepository=" + this.getCuisineCustomersCounterRepository() + ")";
    }


    public Slice<Cuisine> findPaginated(int page, int size) {
        return cuisineRepository.findAll(new PageRequest(page, size));
    }


}
