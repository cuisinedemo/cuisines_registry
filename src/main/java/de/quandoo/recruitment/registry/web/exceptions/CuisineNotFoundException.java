package de.quandoo.recruitment.registry.web.exceptions;

public class CuisineNotFoundException extends RuntimeException {


    private static final long serialVersionUID = 56884566746782455L;

    public CuisineNotFoundException() {
        super();
    }

    public CuisineNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CuisineNotFoundException(final String message) {
        super(message);
    }

    public CuisineNotFoundException(final Throwable cause) {
        super(cause);
    }

}
