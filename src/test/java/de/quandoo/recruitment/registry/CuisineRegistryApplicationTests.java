package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.repositories.AbstractCuisineRepoTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CassandraTestConfig.class)
public class CuisineRegistryApplicationTests extends AbstractCuisineRepoTest {

    @Test
    public void contextLoads() {
    }

}
