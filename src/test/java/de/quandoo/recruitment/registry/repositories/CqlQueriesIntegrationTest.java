package de.quandoo.recruitment.registry.repositories;

import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Tests using cassandra template QueryBuilder
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraTestConfig.class)
@EnableCassandraRepositories("de.quandoo.recruitment.registry.repository")
public class CqlQueriesIntegrationTest extends AbstractCuisineRepoTest {

    public static final String CUSTOMER_TABLE_NAME = "customer";

    @Autowired
    private CassandraAdminOperations adminTemplate;

    @Autowired
    private CassandraOperations cassandraTemplate;

    @AfterClass
    public static void stopCassandraEmbedded() {
        log.info("SHUTTING DOWN Embedded Cassandra Server ...");
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

    @Before
    public void createTable() {
        adminTemplate.createTable(true, CqlIdentifier.of(CUSTOMER_TABLE_NAME), Customer.class, new HashMap<String, Object>());
    }


    @Test
    public void whenSavingCustomer_thenAvailableOnRetrieval_usingQueryBuilder() {

        final UUID uuid = UUID.randomUUID();
        final Insert insert = QueryBuilder.insertInto(CUSTOMER_TABLE_NAME).value("id", uuid).value("name", "Hans");
        cassandraTemplate.getCqlOperations().execute(insert);
        final Select select = QueryBuilder.select().from("customer").limit(10);
        final Customer customerFromDatabase = cassandraTemplate.selectOne(select, Customer.class);
        assertEquals(uuid, customerFromDatabase.getId());

    }

    @Test
    public void whenSavingCustomer_thenAvailableOnRetrieval_usingCQLStatements() {

        final UUID uuid = UUID.randomUUID();
        final String insertCql = "insert into customer (id, name) values " + "(" + uuid + ", 'Kamila')";
        cassandraTemplate.getCqlOperations().execute(insertCql);
        final Select select = QueryBuilder.select().from("customer").limit(10);
        final Customer customerFromDatabase = cassandraTemplate.selectOne(select, Customer.class);
        assertEquals(uuid, customerFromDatabase.getId());

    }

    @After
    public void dropTable() {
        adminTemplate.dropTable(CqlIdentifier.of(CUSTOMER_TABLE_NAME));
    }

}
