package de.quandoo.recruitment.registry.model;

import com.datastax.driver.core.DataType;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static java.util.stream.Collectors.toSet;

/**
 * Customer-Cuisine mapping table. It keeps the track of the cuisines  a customer
 * is registered to
 */
@Table
public class Customer_Cuisine {

    @PrimaryKeyColumn(name = "customer_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    @CassandraType(type = DataType.Name.UUID)
    private UUID customerId;

    @PrimaryKeyColumn(name = "name", ordinal = 1, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private String name;

    @Column
    private Set<UUID> cuisines = new HashSet<>();

    public Customer_Cuisine() {
    }

    public Customer_Cuisine(final UUID customerId, final UUID cuisineID, String customerName) {
        this.customerId = customerId;
        this.name = customerName;
    }

    public Customer_Cuisine(final Customer customer, final Set<Cuisine> cuisines) {
        this.customerId = customer.getId();
        this.name = customer.getName();
        this.cuisines.addAll(cuisines.stream().map(cuisine -> cuisine.getCuisineId()).collect(toSet()));
    }

    public UUID getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UUID> getCuisines() {
        return this.cuisines;
    }

    public void setCuisines(Set<UUID> cuisines) {
        this.cuisines = cuisines;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Customer_Cuisine)) return false;
        final Customer_Cuisine other = (Customer_Cuisine) o;
        if (!other.canEqual(this)) return false;
        final Object this$customerId = this.getCustomerId();
        final Object other$customerId = other.getCustomerId();
        if (this$customerId == null ? other$customerId != null : !this$customerId.equals(other$customerId))
            return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$cuisines = this.getCuisines();
        final Object other$cuisines = other.getCuisines();
        return this$cuisines == null ? other$cuisines == null : this$cuisines.equals(other$cuisines);
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $customerId = this.getCustomerId();
        result = result * PRIME + ($customerId == null ? 43 : $customerId.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $cuisines = this.getCuisines();
        result = result * PRIME + ($cuisines == null ? 43 : $cuisines.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Customer_Cuisine;
    }

    public String toString() {
        return "Customer_Cuisine(customerId=" + this.getCustomerId() + ", name=" + this.getName() + ", cuisines=" + this.getCuisines() + ")";
    }
}
