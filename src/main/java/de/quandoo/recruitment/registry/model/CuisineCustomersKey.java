package de.quandoo.recruitment.registry.model;


import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.io.Serializable;
import java.util.UUID;

/**
 * Key class for cuisine customers tables.
 */
@PrimaryKeyClass
public class CuisineCustomersKey implements Serializable {

    private static final long serialVersionUID = -791316285695123492L;

    @PrimaryKeyColumn(name = "cuisine_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private UUID cuisineId;

    @PrimaryKeyColumn(name = "customer_id", ordinal = 1, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private UUID customerId;

}