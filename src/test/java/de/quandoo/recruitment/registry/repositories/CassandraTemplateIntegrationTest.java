package de.quandoo.recruitment.registry.repositories;

import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.collect.ImmutableSet;
import de.quandoo.recruitment.registry.CassandraTestConfig;
import de.quandoo.recruitment.registry.model.Cuisine;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Template Integration tests. Queries cassandra usint the template
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraTestConfig.class)
public class CassandraTemplateIntegrationTest extends AbstractCuisineRepoTest {

    public static final String DATA_TABLE_NAME = "cuisine";

    @Autowired
    private CassandraAdminOperations adminTemplate;

    @Autowired
    private CassandraOperations cassandraTemplate;

    @AfterClass
    public static void stopCassandraEmbedded() {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
    }

    @Before
    public void createTable() {
        adminTemplate.createTable(true, CqlIdentifier.of(DATA_TABLE_NAME), Cuisine.class, new HashMap<String, Object>());
    }

    @Test
    public void whenSavingCuisine_thenAvailableOnRetrieval() {
        final Cuisine createdCuisine = new Cuisine(UUID.randomUUID(), "Mafia Boss", ImmutableSet.of("Italian", "Greek"));
        cassandraTemplate.insert(createdCuisine);
        final Select select = QueryBuilder.select().from("cuisine").where(QueryBuilder.eq("cuisine_id", createdCuisine.getCuisineId())).limit(10);
        final Cuisine cuisineFromDatabase = cassandraTemplate.selectOne(select, Cuisine.class);
        assertEquals(createdCuisine.getCuisineId(), cuisineFromDatabase.getCuisineId());
        assertEquals(createdCuisine.getTags(), cuisineFromDatabase.getTags());
    }

    @Test
    public void whenSavingCuisines_thenAllAvailableOnRetrieval() {

        final Cuisine thaiCuisine = new Cuisine(UUID.randomUUID(), "Loom Bay Restaurant", ImmutableSet.of("Thai", "Vietnamese"));
        final Cuisine japaneseCuisine = new Cuisine(UUID.randomUUID(), "Brook's", ImmutableSet.of("Vegan", "Vetarian"));

        cassandraTemplate.insert(thaiCuisine);
        cassandraTemplate.insert(japaneseCuisine);
        final Select select = QueryBuilder.select().from("cuisine").limit(10);
        final List<Cuisine> retrievedCuisines = cassandraTemplate.select(select, Cuisine.class);
        assertThat(retrievedCuisines.size(), is(2));

    }


    @Test
    public void whenUpdatingCuisine_thenShouldUpdatedOnRetrieval() {

        final Cuisine createdCuisine = new Cuisine(UUID.randomUUID(), "Marajah", ImmutableSet.of("Indian", "Malai"));
        cassandraTemplate.insert(createdCuisine);
        final Select select = QueryBuilder.select().from("cuisine").limit(10);
        final Cuisine cuisineFromDatabase = cassandraTemplate.selectOne(select, Cuisine.class);
        cuisineFromDatabase.setTags(ImmutableSet.of("Lebanese", "German"));
        cassandraTemplate.update(cuisineFromDatabase);
        final Cuisine updatedFromDatabase = cassandraTemplate.selectOne(select, Cuisine.class);
        assertEquals(cuisineFromDatabase.getTags(), updatedFromDatabase.getTags());

    }

    @Test
    public void whenDeletingASelectedCuisine_thenNotAvailableOnRetrieval() {

        final Cuisine createdCuisine = new Cuisine(UUID.randomUUID(), "Rudi's Ecke", ImmutableSet.of("Deutsch", "German"));
        cassandraTemplate.insert(createdCuisine);
        final Delete delete = QueryBuilder.delete().from("cuisine");
        delete.where(QueryBuilder.eq("cuisine_id", createdCuisine.getCuisineId()));
        delete.where(QueryBuilder.eq("name", createdCuisine.getName()));
        cassandraTemplate.getCqlOperations().execute(delete);
        final Select select = QueryBuilder.select().from("cuisine").limit(10);
        final Cuisine updatedFromDatabase = cassandraTemplate.selectOne(select, Cuisine.class);
        assertNull(updatedFromDatabase);

    }

    @Test
    public void whenDeletingAllCuisines_thenNotAvailableOnRetrieval() {

        final Cuisine createdCuisine = new Cuisine(UUID.randomUUID(), "Hollywood In", ImmutableSet.of("Burger", "TexMex"));
        final Cuisine japaneseCuisine = new Cuisine(UUID.randomUUID(), "OrigotoMoko", ImmutableSet.of("Sushi", "Ramen"));
        cassandraTemplate.insert(createdCuisine);
        cassandraTemplate.insert(japaneseCuisine);
        cassandraTemplate.delete(japaneseCuisine);
        final Select select = QueryBuilder.select().from("cuisine").where(QueryBuilder.eq("cuisine_id", japaneseCuisine.getCuisineId())).limit(1);
        final Cuisine updatedFromDatabase = cassandraTemplate.selectOne(select, Cuisine.class);
        assertNull(updatedFromDatabase);

    }


    @Test
    public void whenAddingCuisines_thenCountShouldBeCorrectOnRetrieval() {

        final Cuisine createdCuisine = new Cuisine(UUID.randomUUID(), "Kotletnaya", ImmutableSet.of("Pelmeni", "Kotleta"));
        final Cuisine japaneseCuisine = new Cuisine(UUID.randomUUID(), "OrigotoMoko", ImmutableSet.of("Sushi", "Ramen"));
        cassandraTemplate.insert(createdCuisine);
        cassandraTemplate.insert(japaneseCuisine);
        final long bookCount = cassandraTemplate.count(Cuisine.class);
        assertEquals(2, bookCount);

    }

    @After
    public void dropTable() {
        adminTemplate.dropTable(CqlIdentifier.of(DATA_TABLE_NAME));
    }
}
